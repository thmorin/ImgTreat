import axios from 'axios'

/**
 * Obtain the list of the images on the servor
 * @param {*} callback the function called when there is no problem. This function take in parameter just the list of images
 * @param {*} callbackError the function called in case of error. This function take in parameter just the error
 */
function callRestServiceGetLstImg(callback, callbackError) {
  axios
    .get('images')
    .then((response) => {
      // JSON responses are automatically parsed.
      callback(response.data)
    })
    .catch((e) => {
      callbackError(e)
    })
}

/**
 * Obtain an image on the servor
 * @param {*} id the id of the image that you would like
 * @param {*} callback the function called when there is no problem. This function take in parameter an object in which the response is as .result
 * @param {*} data the object which is going to be augmented by the result of the request and passed to the callback function
 * @param {*} callbackError the function called when there is a problem. This function take in parameter just the error
 */
function callRestServiceGetImg(id, callback, data, callbackError) {
  const url = '/images/' + id
  axios.get(url, {
      responseType: 'blob'
    })
    .then((response) => {
      var reader = new window.FileReader()
      reader.readAsDataURL(response.data)
      reader.onload = function () {
        data.result = reader.result
        callback(data)
      }
    }).catch((e) => {
      callbackError(e)
    })
}

/**
 * Delete an image on the servor
 * @param {*} id the id of the image that you want delete
 * @param {*} callbackError the function called when there is an error. This function take in parameter just the error
 */
function callRestServiceDeleteImg(id, callbackError) {
  const url = '/images/' + id
  axios.delete(url).catch((e) => {
    callbackError(e)
  })
}
/**
 * Post an image on te servor
 * @param {*} data the data corresponding to the image that you would like post
 * @param {*} callback The function called where there is no problem. This function doesn't take any parameter
 * @param {*} callbackError The function called where there is a problem. This function take the error on parameter
 */
function callRestServicePostImg(data, callback, callbackError) {
  axios.post('/images',
    data
  ).then(function () {
    if (callback != null) {
      callback()
    }
  }).catch((e) => {
    if (callbackError != null) {
      callbackError(e)
    }
  })
}

/**
 * Get the info of an image
 * @param {*} id The id of the image that you want the infos
 * @param {*} callback the function called when there is no problem. This function take in parameter an object corresponding to the info
 * And a function which is going to be called when there is error
 * @param {*} callbackError A function called in case of error
 */
function callRestServiceGetInfoImg(id, callback, callbackError) {
  const url = '/images/infos/' + id
  axios.get(url, {}).then((response) => {
    callback(response.data, callbackError)
  }).catch((e) => {
    callbackError(e)
  })
}

/**
 * Increase the luminosity of an image
 * @param {*} id The id of the image that you like to increase the luminosity
 * @param {*} callback The function when there is no problem. THis function take in parameter an object in which there a result field corresponding
 * To the data image back
 * @param {*} callbackError The function called in case of error. This function take in parameter the type of the error
 */
function callRestServiceIncreaseLuminosity(id, delta, callback, callbackError, isPreview) {
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?' + chainalgo +'=increaseLuminosity&p1=gain=' + delta + '&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}
/**
 * Histogram equalization
 * @param {*} id The id of the image that you like to equalize the histogram
 * @param {*} callback The function when there is no problem. THis function take in parameter an object in which there a result field corresponding
 * To the data image back
 * @param {*} callbackError The function called in case of error. This function take in parameter the type of the error
 */
function callRestServiceHistogramEqualization(id, canal, callback, callbackError, isPreview) {
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?' + chainalgo +'=histogramEqualization&p1=channel=' + canal + '&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

/**
 * Colored filter
 * @param {*} id The id of the image that you like to color
 * @param {*} callback The function when there is no problem. THis function take in parameter an object in which there a result field corresponding
 * To the data image back
 * @param {*} callbackError The function called in case of error. This function take in parameter the type of the error
 */
function callRestServiceColoredFilter(id, newColor, callback, callbackError, isPreview) {
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+ chainalgo +'=coloredFilter&p1=hue=' + newColor + '&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}
/**
 * Blurry Filter
 * @param {*} id The id of the image that you like to blur
 * @param {*} callback The function when there is no problem. THis function take in parameter an object in which there a result field corresponding
 * To the data image back
 * @param {*} callbackError The function called in case of error. This function take in parameter the type of the error
 */
function callRestServiceBlurryFilter(id, type, size, callback, callbackError, isPreview) {
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+ chainalgo+'=blurryFilter&p1=type=' + type + '&p2=size=' + size
  callRestServiceFromUrl(url, callback, callbackError)
}

/**
 * Sobel Filter
 * @param {*} id The id of the image that you like to have the filter
 * @param {*} callback The function when there is no problem. THis function take in parameter an object in which there a result field corresponding
 * To the data image back
 * @param {*} callbackError The function called in case of error. This function take in parameter the type of the error
 */
function callRestServiceSobelFilter(id, isGrayLevelApplication, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=sobelFilter&p1=colored=' + isGrayLevelApplication+ '&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceCannyFilter(id, isGrayLevelApplication, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=cannyFilter&p1=colored=' + isGrayLevelApplication+ '&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceCartoonEffect(id, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=cartoonEffect&p1=X&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceSketchEffect(id, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=sketchEffect&p1=X&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceOilPaintingEffect(id, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=oilPaintingEffect&p1=X&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceErosionEffect(id, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=erosionEffect&p1=X&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceDilatationEffect(id, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=dilatationEffect&p1=X&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceResizeImg(id, newSize, callback, callbackError){
  const url = '/images/' + id + '?algorithm=resize&p1=factor=' + newSize+ '&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceRotateImg(id, angle, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=rotate&p1=angle=' + angle+ '&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceAddImg(id, idSecondImg, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=add&p1=id=' + idSecondImg+ '&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceSubstractImg(id, idSecondImg, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=substract&p1=id=' + idSecondImg+ '&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceDivideImg(id, idSecondImg, callback, callbackError, isPreview){
  const chainalgo = isPreview ? "previewOneAlgorithm" : "algorithm"
  const url = '/images/' + id + '?'+chainalgo+'=divide&p1=id=' + idSecondImg+ '&p2=Y'
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceSeamCarving(id, newWidth, newHeight, callback, callbackError){
  console.log(newWidth +" "+newHeight) ;
  const url = '/images/' + id + '?algorithm=seamK&p1=width=' + newWidth+ '&p2=height='+newHeight
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceSeamCW(id, newWidth, newHeight, callback, callbackError){
  console.log(newWidth +" "+newHeight) ;
  const url = '/images/' + id + '?algorithm=seamCW&p1=width=' + newWidth+ '&p2=height='+newHeight
  callRestServiceFromUrl(url, callback, callbackError)
}


function callRestServiceApplyListAlgo(id, values, callback, callbackError) {
  let lstAlgo = '/images/' + id+'?algos=' 
  for(let i = 0 ; i < values.length ; i++){
    const algo = values[i]
    if (algo["name"] === undefined)
      callbackError("undefined")
    else
      lstAlgo+=algo["name"]
    if (algo["param1"] != undefined){
      lstAlgo+=','
      lstAlgo+=algo["param1"]
    }
    if (algo["param2"] != undefined){
      lstAlgo+=','
      lstAlgo+=algo["param2"]
    }
    lstAlgo+=";"
  }
  callRestServiceFromUrl(lstAlgo, callback, callbackError)
}

function callRestServiceApplyPreview(id, values, callback, callbackError) {
  let lstAlgo = '' 
  for(let i = 0 ; i < values.length ; i++){
    const algo = values[i]
    if (algo["name"] === undefined)
      callbackError("undefined")
    else
      lstAlgo+=algo["name"]
    if (algo["param1"] != undefined){
      lstAlgo+=','
      lstAlgo+=algo["param1"]
    }
    if (algo["param2"] != undefined){
      lstAlgo+=','
      lstAlgo+=algo["param2"]
    }
    lstAlgo+=";"
  }
  const url = 'images/'+ id +'?preview=' + lstAlgo
  callRestServiceFromUrl(url, callback, callbackError)
}

function callRestServiceFromUrl(url, callback, callbackError){
  axios.get(url, {
    responseType: 'blob'
  })
  .then((response) => {
    var reader = new window.FileReader()
    reader.readAsDataURL(response.data)
    reader.onload = function () {
      callback(reader) // this is something like really uggly thing
    }
  }).catch((e) => {
    callbackError(e)
  })
}

export {
  callRestServiceSeamCW,
  callRestServiceSeamCarving,
  callRestServiceOilPaintingEffect,
  callRestServiceSketchEffect,
  callRestServiceDilatationEffect,
  callRestServiceErosionEffect,
  callRestServiceCartoonEffect,
  callRestServiceApplyPreview,
  callRestServiceApplyListAlgo,
  callRestServiceAddImg,
  callRestServiceSubstractImg,
  callRestServiceDivideImg,
  callRestServiceRotateImg,
  callRestServiceBlurryFilter,
  callRestServiceResizeImg,
  callRestServiceColoredFilter,
  callRestServiceHistogramEqualization,
  callRestServiceIncreaseLuminosity,
  callRestServiceGetInfoImg,
  callRestServicePostImg,
  callRestServiceDeleteImg,
  callRestServiceGetLstImg,
  callRestServiceGetImg,
  callRestServiceSobelFilter,
  callRestServiceCannyFilter
}