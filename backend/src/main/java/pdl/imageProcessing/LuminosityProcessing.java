package pdl.imageProcessing;

import net.imglib2.RandomAccess;
import net.imglib2.Cursor ;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import java.util.ArrayList;
import java.util.List;

import net.imglib2.view.Views;
import net.imglib2.view.IntervalView;
import net.imglib2.loops.LoopBuilder;

/**
 * This class factorize some methods of image processing which concerns problems of luminosity (changements, means)
 */
public class LuminosityProcessing{


    /**
     * change by a delta term all the luminosity of an image with a use of a loop multithreaded
     * @param img the image
     * @param delta the change of luminosity
     */
	public static void changeLuminosityLoop(Img<UnsignedByteType> img, int delta){
		LoopBuilder.setImages(img).multiThreaded().forEachPixel(
			pix -> {
				if (pix.get() + delta > 255) pix.set(255) ;
				else if (pix.get() + delta < 0) pix.set(0) ;
				else pix.set(pix.get() + delta) ;
			}
		);
	}

    /**
     * change by a delta term all the luminosity of a gray level image with a use of a random access
     * @param img the gray level image
     * @param delta the change of luminosity
     */
	public static void changeLuminosityRandomAccessGrayLevel(Img<UnsignedByteType> img, int delta){
		final RandomAccess<UnsignedByteType> r = img.randomAccess();

		final int iw = (int) img.max(0);
		final int ih = (int) img.max(1);

		for (int x = 0; x <= iw; ++x) {
			for (int y = 0; y <= ih; ++y) {
				r.setPosition(x, 0);
				r.setPosition(y, 1);
				int val = r.get().get();
				if (val + delta > 255) r.get().set(255) ;
				else if (val + delta < 0) r.get().set(0) ;
				else r.get().set(val + delta) ;
			}
		}
	}

    /**
     * change by a delta term all the luminosity of a colored image with a use of a random access
     * @param img the colored image
     * @param delta the change of luminosity
     */
	public static void changeLuminosityRandomAccessColor(Img<UnsignedByteType> img, int delta){
		RandomAccess<UnsignedByteType> r = Views.hyperSlice(img, 2, 0).randomAccess(), g = Views.hyperSlice(img, 2, 1).randomAccess(),
		 b = Views.hyperSlice(img, 2, 2).randomAccess() ;

		 List<RandomAccess<UnsignedByteType>> randomAccesses = new ArrayList<>() ;
		 randomAccesses.add(r) ; randomAccesses.add(g) ; randomAccesses.add(b) ;

		final int iw = (int) img.max(0);
		final int ih = (int) img.max(1);

		for (int x = 0; x <= iw; ++x) {
			for (int y = 0; y <= ih; ++y){
				for (RandomAccess<UnsignedByteType> ra : randomAccesses){
					ra.setPosition(x, 0);
					ra.setPosition(y, 1);

					int val = ra.get().get();
					if (val + delta > 255) ra.get().set(255) ;
					else if (val + delta < 0) ra.get().set(0) ;
					else ra.get().set(val + delta) ;
				}
			}
		}
	}

    /**
     * change by a delta term all the luminosity of an image with a use of a cursor
     * @param img the image
     * @param delta the change of luminosity
     */
	public static void changeLuminosityCursorClassic(Img<UnsignedByteType> img, int delta){
		final Cursor<UnsignedByteType> c = img.cursor() ;

		while(c.hasNext()){
			c.fwd();
			int val = c.get().get();
			if (val + delta > 255) c.get().set(255) ;
			else if (val + delta < 0) c.get().set(0) ;
			else c.get().set(val + delta) ;
		}
	}

    /**
     * compute the mean of luminosity of a given image
     * @param img the given image
     */
	public static long computeMeanLuminosity(Img<UnsignedByteType> img){
		long sum = 0 ;
		final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0), inputG = Views.hyperSlice(img, 2, 1), inputB = Views.hyperSlice(img, 2, 2);
		final Cursor<UnsignedByteType> cursorR =inputR.cursor(), cursorG = inputG.cursor(), cursorB = inputB.cursor() ;
		while (cursorR.hasNext() && cursorG.hasNext() && cursorB.hasNext()){
			cursorR.fwd(); cursorG.fwd(); cursorB.fwd();
			int value = (int) (0.3*cursorR.get().get() + 0.59*cursorG.get().get() + 0.11*cursorB.get().get()) ;
			sum += value ;
		}
		return sum / img.size() ;
	}


}
