package pdl.imageProcessing;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import java.util.function.BiFunction ;
import java.util.function.Function;
/**
 * This class stores some image processing of arithmetical or logical operation on images
 */
public class OperationImgProcessing {

    /**
     * Take in parameters two images of the same number of slices
     * and return an image which result of applying the bifunction on each pixel when the pixel considered is valid on the two images
     * or the corresponding function when just one is valid
     * @param img1 the first image of the operation
     * @param img2 the second image of the operation
     * @param output the result of the operation
     * @param functionWhenAllValid the function to apply on each pixel which has a valid position on the two images
     * @param functionWhenFirstValid the function to apply on each pixel which has a valid position just on the first image
     * @param functionWhenSecondValid the function to apply on each pixel which has a valid position just on the second image
     */
    public static void binaryOperation(Img<UnsignedByteType> img1, Img<UnsignedByteType> img2, Img<UnsignedByteType> output, BiFunction<Integer, Integer, Integer> functionWhenAllValid, Function<Integer, Integer> functionWhenFirstValid, Function<Integer, Integer> functionWhenSecondValid ){
        RandomAccess<UnsignedByteType> random1 = img1.randomAccess(), random2 = img2.randomAccess() ;
        Cursor<UnsignedByteType> cOutput = output.cursor() ;

        long nbDimensions = output.numDimensions() ;
        long position [] = new long [(int)nbDimensions] ;


        long maxImg1 [] = new long [(int)nbDimensions], maxImg2 [] = new long [(int)nbDimensions] ;

        for(int i = 0; i < nbDimensions ; i++){
            // computing the dimensions
            maxImg1[i] = img1.max(i) ;
            maxImg2[i] = img2.max(i) ;
        }

        while (cOutput.hasNext()){
            cOutput.fwd() ;
            for(int i = 0; i < nbDimensions ; i++){
                position[i] = cOutput.getIntPosition(i) ;
            }
            boolean isPixelValid1 = true, isPixelValid2 = true  ;
            for(int i = 0; i < nbDimensions ;i++){
                if (position[i] > maxImg1[i] )
                    isPixelValid1 = false ;
                else if (isPixelValid1)
                    random1.setPosition(position[i], i) ;
                if (position[i] > maxImg2[i])
                    isPixelValid2 = false ;
                else if (isPixelValid2)
                    random2.setPosition(position[i], i) ;
            }
            if (isPixelValid1 && isPixelValid2)
                cOutput.get().set(functionWhenAllValid.apply(random1.get().get(), random2.get().get()) ) ;
            else if (isPixelValid1)
               cOutput.get().set(functionWhenFirstValid.apply(random1.get().get())) ;
            else
                cOutput.get().set(functionWhenSecondValid.apply(random2.get().get())) ;
        }
    }

    /**
     * Add two images by meaning the two values of each pixel
     * @param img1 the first image to add
     * @param img2 the second image to add
     * @param output the result of the adding
     */
    public static void addImg(Img<UnsignedByteType> img1, Img<UnsignedByteType> img2, Img<UnsignedByteType> output){
        binaryOperation(img1, img2, output, (pix1, pix2) -> (pix1+pix2)/2, (pix1) -> pix1, (pix2) -> pix2);
    }
    /**
     * Substract two images by absolute the substraction of each pixel
     * @param img1 the first image to substract
     * @param img2 the second image to substract
     * @param output the result of the substraction
     */
    public static void subImg(Img<UnsignedByteType> img1, Img<UnsignedByteType> img2, Img<UnsignedByteType> output){
        binaryOperation(img1, img2, output, (pix1, pix2) -> Math.abs(pix1 - pix2), (pix1) -> pix1, (pix2) -> 255-pix2);
    }
    /**
     * Divide two images
     * @param img1 the first image of the division
     * @param img2 the second image of the division
     * @param output the result of the division
     */
    public static void divideImg(Img<UnsignedByteType> img1, Img<UnsignedByteType> img2, Img<UnsignedByteType> output){
        binaryOperation(img1, img2, output, (pix1, pix2) ->pix1/ (pix2+1), (pix) -> pix, (pix) -> 0);
        if (output.numDimensions() > 2)
            ContrastProcessing.dynamicExtensionColor_ValueChannel(output, 0, 100); // On repartit les valeurs
        else
            ContrastProcessing.dynamicExtensionGrayLevel(output, 0, 255);
    }
    /**
     * Inverse a given image by transforming each pixel of value i by 255 - i
     * @param img the given image
     * @param output the result of the inversion
     */
    public static void negative(Img<UnsignedByteType> img, Img<UnsignedByteType> output){
        final Cursor<UnsignedByteType> cInput = img.cursor(), cOutput = output.cursor() ;
        while (cInput.hasNext() && cOutput.hasNext()){
            cInput.fwd(); cOutput.fwd() ;
            cOutput.get().set(255 - cInput.get().get()) ;
        }
    }
}

