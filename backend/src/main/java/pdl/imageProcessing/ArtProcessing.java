package pdl.imageProcessing;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;


import net.imglib2.Cursor;
import net.imglib2.RandomAccess;

/**
 * This class factorize some methods for giving to an image an art effect
 */
public class ArtProcessing {

    /**
     * This algorithm reduce the number of colors of each slice to nbColors.
     * If the number of slices is 2, there is just nbColors colors after
     * If the number of slices is 3, there is nbColors^3 colors after
     * @param img the image to reduce
     * @param nbColors the final number of colors that you want
     */
    public static void colorReduction(Img<UnsignedByteType> img, int nbColors){
        int nbDimensions = img.numDimensions() ;
        int factor = 256/nbColors ;
        RandomAccess<UnsignedByteType> rImg = img.randomAccess() ;

        for(int x = 0 ; x <= img.max(0) ; x++){
            for (int y = 0 ; y <= img.max(1) ; y++){
                for(int z = 0 ; z < (nbDimensions == 3 ? 3 : 1) ; z++){
                    if (nbDimensions == 3)
                        rImg.setPosition(z, 2);
                    rImg.setPosition(x, 0) ;
                    rImg.setPosition(y, 1);
                    int oldPix = rImg.get().get() ;
                    int newPix = (int) Math.round( Double.valueOf(oldPix/factor) *factor) ;
                    rImg.get().set(newPix) ;
                }
            }
        }
    }

    /**
     * This function is used to give a cartoon effect to an image
     * The idea is simple : we reduct the number of colors and after we mark in black the contour of the image
     */
    public static void cartoonEffect(Img<UnsignedByteType> img) {
        // first we reduce the number of colors of the image
        colorReduction(img, 5) ;

        // now we're going to improve the contours of the image
        Img<UnsignedByteType> imgGray = img.copy(), cannyImg = img.copy() ;
        if (img.numDimensions() == 3)
            Convolution.cannyFilterColor(imgGray, cannyImg, true);
        else
            Convolution.cannyFilterGrayLevel(imgGray, cannyImg);
        // we are setting to black each pixel which is > 100 on the canny Img
        Cursor<UnsignedByteType> cursCanny = cannyImg.cursor() ;
        RandomAccess<UnsignedByteType> randImg = img.randomAccess() ;
        while(cursCanny.hasNext()){
            cursCanny.fwd() ;

            if (cursCanny.get().get() > 0){
                randImg.setPosition(cursCanny.getIntPosition(0), 0);
                randImg.setPosition(cursCanny.getIntPosition(1), 1);
                if (img.numDimensions() == 3)
                    randImg.setPosition(cursCanny.getIntPosition(2), 2);
                randImg.get().set(0) ;
            }
        }
    }
    /**
     * This function give to an image an effect of sketch. The result is on gray levels
     * The algorithm is really simple : divide an image version with high dynamic by an image version with low dynamic, all this in gray level
     * @param img the image to sketch
     */
    public static void sketchEffect(Img<UnsignedByteType> img) {
        if (img.numDimensions() == 3){
            // Pour faire l'esquisse d'une image, on fait la division de l'image en noir et blanc avec une grande image par l'image avec peu de dynamique

            Img<UnsignedByteType> copy = img.copy(), gl = ColorUtilitariesProcessing.Color3DimsImgTo1DimImg(copy) ;
            Img<UnsignedByteType> lowLevDyn = gl.copy(), highLevDyn = gl.copy() ;
            ContrastProcessing.dynamicExtensionGrayLevel(lowLevDyn, 0, 50);
            ContrastProcessing.dynamicExtensionGrayLevel(highLevDyn, 0, 255);
            OperationImgProcessing.divideImg(highLevDyn, lowLevDyn, gl);

            // On a dans gl une esquisse de l'image. Maintenant il faut la retablir dans img

            Cursor<UnsignedByteType> cGraylevel = gl.cursor() ;
            RandomAccess<UnsignedByteType> rImg = img.randomAccess() ;

            while(cGraylevel.hasNext()){
                cGraylevel.fwd();
                rImg.setPosition(cGraylevel.getIntPosition(0), 0);
                rImg.setPosition(cGraylevel.getIntPosition(1), 1);
                int valPix = cGraylevel.get().get() ;
                for(int i = 0; i < 3; i++){
                    rImg.setPosition(i, 2);
                    rImg.get().set(valPix) ;
                }
            }
        }
        else {
            // meme principe mais sans retablir : il suffit de faire une image en faible dynamique, une image en haute dynamique et de les diviser dans img
            Img<UnsignedByteType> lowLevDyn = img.copy(), highLevDyn = img.copy() ;
            ContrastProcessing.dynamicExtensionGrayLevel(lowLevDyn, 0, 50);
            ContrastProcessing.dynamicExtensionGrayLevel(highLevDyn, 0, 255);
            OperationImgProcessing.divideImg(highLevDyn, lowLevDyn, img);
        }
    }

    /**
     * Give an oil Painting effect to an image. The idea is simple : reduct the number of colors and after apply a medianFilter of well choosen size
     * @param input
     * @param output
     */
    public static void oilPaintingEffect(Img<UnsignedByteType> input, Img<UnsignedByteType> output){
        // l'effet oil painting est un peu idiot : on reduit les couleurs et on applique un gros filtre median
        colorReduction(input, 8);

        // On a remarque que pour une image de taille 1920 en width, un filtre de tailel 9 etait bon.
        // 1920 / 9 ~= 215 ; ainsi on prend cette constante
        long constant = input.max(0) / 215L ;
        constant = Math.max(constant %2 == 0 ? constant+1 : constant, 3) ;
        Convolution.medianFilter(input, output, (int) constant);
    }

}