package pdl.imageProcessing;

import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;

/**
 * This class store some methods which doesn't have they locations in other class
 */
public class OthersProcessing {

    /**
     * Perform a pixellization of a given gray level image
     * @param img the given gray level image
     * @param output the result of the pixellization
     * @param blocSize the size of each block of the pixellization
     */
    public static void pixellizationGrayLevel(Img<UnsignedByteType> img, Img<UnsignedByteType> output, int blocSize){
        if (blocSize <= 0){
            System.err.println("error : blocSize has to be positive") ;
            return ;
        }
        final RandomAccess<UnsignedByteType> rInput = img.randomAccess(), rOutput = output.randomAccess() ;
        long maxW = img.max(0), maxH = img.max(1) ;
        for(int x = 0 ; x <= maxW ; x += blocSize){
            for(int y = 0 ; y <= maxH ; y += blocSize){
                long sum = 0, nbPix = 0 ;
                for(int i = 0 ; i < blocSize && x + i <=maxW ; i++){
                    for(int j = 0 ; j < blocSize && y + j <= maxH ; j++){
                        rInput.setPosition(x+i, 0);
                        rInput.setPosition(y+j, 1);
                        sum+=rInput.get().get() ;
                        nbPix++ ;
                    }
                }
                sum/=nbPix ;
                for(int i = 0 ; i < blocSize && x + i <=maxW ; i++){
                    for(int j = 0 ; j < blocSize && y + j <= maxH ; j++){
                        rOutput.setPosition(x+i, 0);
                        rOutput.setPosition(y+j, 1);
                        rOutput.get().set((int)sum) ;
                    }
                }
            }
        }
    }
}

