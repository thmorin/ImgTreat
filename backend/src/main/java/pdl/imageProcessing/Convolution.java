package pdl.imageProcessing;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

import net.imglib2.view.Views;
import pdl.imageProcessing.exception.InternalException;
import net.imglib2.view.IntervalView;
import net.imglib2.algorithm.gauss3.Gauss3;
import net.imglib2.util.Intervals;
import net.imglib2.Interval;
import net.imglib2.IterableInterval;
/**
 * This class factorize all the code to make differents types of convolution on colored or Gray level images
 */
public class Convolution {

	/**
	 * Apply a mean of a given size on a given image. The image is extended by the use of a mirror double.
	 * The filter is applied on an other image : the input param is not changed
	 * @param input the input image that you want to mean
	 * @param output the result of the mean filter
	 * @param size the size (diameter) of the mean
	 */
	public static void meanFilter(final Img<UnsignedByteType> input, final Img<UnsignedByteType> output, int size) {
		BiFunction<Integer, List<Integer>, Integer> func = (value, neighboors) -> {
			int sum = 0 ;
			for(int valNeigh : neighboors){
				sum+=valNeigh ;
			}
			return sum/neighboors.size() ;
		};

		if (input.dimension(1) <= 50)
			genericFilter(input, output, size, func) ;
		else 
			genericFilterMultithreaded(input, output, size, func);
	}
	/**
	 * Apply a kernel filter on a given image.
	 * The filter is applied on an other image : the input param is not changed
	 * @param input the given gray level image
	 * @param output the result of the filter
	 * @param kernel the given kernel to apply
	 * @throws InternalException
	 */
	public static void convolution(final Img<UnsignedByteType> input, final Img<UnsignedByteType> output,
			int[][] kernel) throws InternalException {
				final int kWidth = kernel[0].length, kHeight = kernel.length; //getting the size
				if (kWidth != kHeight)
					throw new InternalException("différence de taille du noyau de convolution") ;
				BiFunction<Integer, List<Integer>, Integer> func = (pix, neighboors) -> {
					int sum = 0 ;
					for(int i = 0 ; i < kWidth ; i++){
						for(int j = 0; j < kWidth; j++){
							sum+=(neighboors.get(i*kWidth+j)*kernel[i][j]) ;
						}
					}
					return sum/(kWidth*kWidth) ;
				};
				if (input.dimension(1) <= 50)
					genericFilter(input, output, kWidth, func) ;
				else 
					genericFilterMultithreaded(input, output, kWidth, func);
	}

	/**
	 * This class is used to parallelize the treatment of the filter application :
	 * The classical setImage could'nt be used because of the need of the localization of the pixel.
	 */
	private static class ChunkGenericFilter extends Thread {
		RandomAccess<UnsignedByteType> input ;
		Cursor<UnsignedByteType> output ;
		int size ;
		boolean isColored ;
		BiFunction<Integer, List<Integer>, Integer> functionToApplyToNeighs ;
		/**
		 * initiate a generic filter on a chunk of an image
		 * @param input a random access on the image. The filter is going to access on the interval [minOutput-size/2, maxOutput+size/2], give a good randomAccess
		 * To the image
		 * @param output the chunk of the image in which you apply the filter
		 * @param size the size of the filter
		 * @param functionToApplyToNeighs The function to apply to each neighborhood of a pixel. The arrayList parameter corresponds to the neighborhood of the pixel, the integer parameter
		 * corresponds to the value of the pixel on the old image ; the return value corresponds to the new value of the pixel
		 */
		ChunkGenericFilter(RandomAccess<UnsignedByteType> input, Cursor<UnsignedByteType> output, int size, BiFunction<Integer, List<Integer>, Integer> functionToApplyToNeighs){
			this.input = input ; this.output = output ;
			this.size = size ; this.functionToApplyToNeighs = functionToApplyToNeighs ;
			isColored = input.numDimensions() == 3 ;
		}


		@Override
		public void run() {
			while(output.hasNext()){
				output.fwd() ;
				int x = 0, y = 0 ;
				List<Integer> neighboors = new ArrayList<>(size*size) ;
				x = output.getIntPosition(0) ; y = output.getIntPosition(1) ;
				if (isColored)
					input.setPosition(output.getIntPosition(2), 2);
				input.setPosition(x, 0) ;
				input.setPosition(y, 1);
				int value = input.get().get() ;
				for(int dx = -size/2; dx <= size/2; dx++){
					for(int dy = -size/2 ; dy <= size/2 ; dy++){
						input.setPosition(x+dx, 0);
						input.setPosition(y+dy, 1);
						neighboors.add(input.get().get()) ;
					}
				}
				output.get().set(functionToApplyToNeighs.apply(Integer.valueOf(value), neighboors)) ;
			}
		}
	}

	/**
	 * This method is used to apply generically a filter to an image input and put the result on the image output
	 * The idea is to, for each pixel, apply a function on a arrayList which correspond to all the pixels on the neighborood of
	 * the central pixels. The values of the pixels are put on order of apparition (ie the line after line). This method works on gray level and colored images
	 * @param input The image to apply a filter
	 * @param output the result of the filter
	 * @param size the size of the filter
	 * @param functionToApplyToNeighs the function to apply to the neighborhood. The first parameter is the value of the pixel x, y on the input image ; the second is
	 * the list of the values of the pixels which are on a neighborhood of -size/2, size/2 of the x, y pixel ; the result if the value to assign to the x, y pixel on the outptu image
	 */
	private static void genericFilter(final Img<UnsignedByteType> input, final Img<UnsignedByteType> output, int size, BiFunction<Integer, List<Integer>, Integer> functionToApplyToNeighs){
		IntervalView<UnsignedByteType> expandedView  ; // O corresponds to the color dimnesion
		boolean isColored = input.numDimensions() == 3 ;
		if (isColored)
			expandedView = Views.expandMirrorDouble(input, size/2, size/2, 0) ; // image couleur
		else
			expandedView = Views.expandMirrorDouble(input, size/2, size/2) ;
		final RandomAccess<UnsignedByteType> rInput = expandedView.randomAccess() ;
		final Cursor<UnsignedByteType> cOutput = output.cursor() ;

		while(cOutput.hasNext()) {
			cOutput.fwd() ;
			List<Integer> neighboors = new ArrayList<>(size*size) ;
			int x = cOutput.getIntPosition(0), y = cOutput.getIntPosition(1) ;
			if (isColored)
				rInput.setPosition(cOutput.getIntPosition(2), 2);
			rInput.setPosition(x, 0);
			rInput.setPosition(y, 1);
			int value = rInput.get().get() ;
			for(int dx = -size/2; dx <= size/2; dx++){
				for(int dy = -size/2 ; dy <= size/2 ; dy++){
					rInput.setPosition(x+dx, 0);
					rInput.setPosition(y+dy, 1);
					neighboors.add(rInput.get().get()) ;
				}
			}
			cOutput.get().set(functionToApplyToNeighs.apply(Integer.valueOf(value), neighboors)) ;
		}
	}

	/**
	 * This method is used to apply generically a filter to an image input and put the result on the image output
	 * The idea is to, for each pixel, apply a function on a arrayList which correspond to all the pixels on the neighborood of
	 * the central pixels. The values of the pixels are put on order of apparition (ie the line after line). This method works on gray level and colored images
	 * @param input The image to apply a filter
	 * @param output the result of the filter
	 * @param size the size of the filter
	 * @param functionToApplyToNeighs the function to apply to the neighborhood. The first parameter is the value of the pixel x, y on the input image ; the second is
	 * the list of the values of the pixels which are on a neighborhood of -size/2, size/2 of the x, y pixel ; the result if the value to assign to the x, y pixel on the outptu image
	 */
	private static void genericFilterMultithreaded(final Img<UnsignedByteType> input, final Img<UnsignedByteType> output, int size, BiFunction<Integer, List<Integer>, Integer> functionToApplyToNeighs){
		IntervalView<UnsignedByteType> expandedView  ;
		boolean isColored = input.numDimensions() == 3 ;
		if (isColored)
			expandedView = Views.expandMirrorDouble(input, size/2, size/2, 0) ; // image couleur
		else
			expandedView = Views.expandMirrorDouble(input, size/2, size/2) ;

		final int nbThreads = 8 ;
		final long sliceSize = output.dimension(1)/nbThreads ;
		ArrayList<ChunkGenericFilter> chunks = new ArrayList<>(nbThreads) ;

		for(int i = 0 ; i < nbThreads ; i++ ){
			IterableInterval<UnsignedByteType> interv ;
			if (isColored)
				interv = Views.interval(output, new long [] {0, i*sliceSize, 0}, new long [] {output.max(0), (i+1)*sliceSize - 1, 2} ) ;
			else
				interv = Views.interval(output, new long [] {0, i*sliceSize}, new long [] {output.max(0), (i+1)*sliceSize - 1} ) ;
			RandomAccess<UnsignedByteType> rInput = expandedView.randomAccess() ;
			ChunkGenericFilter chunk = new Convolution.ChunkGenericFilter(rInput, interv.cursor(), size, functionToApplyToNeighs) ;
			chunk.start();
			chunks.add(chunk) ;
		}
		for(int i = 0 ; i < nbThreads ; i++){
			try {
				chunks.get(i).join() ;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}



	/**
	 * Apply a Sobel filter (contour filter) on a given image
	 * @param input the given image
	 * @param output the result of the filter
	 */
	public static void sobelFilter(final Img<UnsignedByteType> input, final Img<UnsignedByteType> output, boolean isGrayLevel){
		if (input.numDimensions() == 3 && isGrayLevel) ColorUtilitariesProcessing.colorToGrayLBMultithreaded(input);

		IntervalView<UnsignedByteType> expandedView ;
		if (input.numDimensions() == 3){
			if (isGrayLevel)
				ColorUtilitariesProcessing.colorToGrayLBMultithreaded(input);
			expandedView = Views.expandZero(input, 1, 1, 0) ; // the size of the filter (1) ; expanding on zero to detect the contours of the img

		}
		else {
			expandedView = Views.expandZero(input, 1, 1) ; // the size of the filter (1) ; expanding on zero to detect the contours of the img

		}
		final RandomAccess<UnsignedByteType> rInput = expandedView.randomAccess() ;
		final Cursor<UnsignedByteType> cOutput = output.cursor();

		int [] [] grad = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}} ;

		while(cOutput.hasNext()){
			cOutput.fwd() ;
			int x = cOutput.getIntPosition(0), y = cOutput.getIntPosition(1), z = cOutput.getIntPosition(2) ;
			int sumX = 0, sumY = 0 ;
			if (input.numDimensions() == 3)
				rInput.setPosition(z, 2) ;
			for(int i = -1 ; i <= 1; i++){
				for(int j = -1 ; j <= 1; j++){
					rInput.setPosition(x+i, 0);
					rInput.setPosition(y+j, 1);
					sumX+= rInput.get().get() * grad[i+1][j+1] ;
					sumY+=rInput.get().get() * grad[j+1][i+1] ;
				}
			}
			cOutput.get().set((int) Math.sqrt (sumX*sumX + sumY*sumY)) ;
		}
	}

	/**
	 * Apply a median filter on a given image
	 */
	public static void medianFilter(final Img<UnsignedByteType> input, final Img<UnsignedByteType> output, int diameter){

		genericFilter(input, output, diameter, (value, neighboors) -> {
			Collections.sort(neighboors) ;
			return neighboors.get(diameter*diameter/2) ;
		});
		BiFunction<Integer, List<Integer>, Integer> func = (value, neighboors) -> {
			Collections.sort(neighboors) ;
			return neighboors.get(diameter*diameter/2) ;
		} ;
		if (input.dimension(1) <= 50)
			genericFilter(input, output, diameter, func) ;
		else 
			genericFilterMultithreaded(input, output, diameter, func);

	}
	/**
	 * Apply a bilateral filter on an image. The max difference to be accepted by this method if 50 for the moment
	 */
	public static void bilateralFilter(final Img<UnsignedByteType> input, final Img<UnsignedByteType> output, int diameter){
		BiFunction<Integer, List<Integer>, Integer> func = (value, neighboors) -> {
			int sum = 0, nbVals = 0 ;
			for(int pixVal : neighboors){
				if (Math.abs(pixVal - value) < 50){
					sum+=pixVal ;
					nbVals++ ;
				}
			}
			return sum/nbVals ;
		} ;
		if (input.dimension(1) <= 50)
			genericFilter(input, output, diameter, func) ;
		else 
			genericFilterMultithreaded(input, output, diameter, func);
	}



	/**
	 * Apply a Canny filter (contour filter) on a given gray level image ; not finished : to do better
	 * @param input the given gray level image
	 * @param output the result of the filter
	 */
	public static void cannyFilterGrayLevel(final Img<UnsignedByteType> input, final Img<UnsignedByteType> output){
		final Img<UnsignedByteType> inputWithoutNoise = input.copy() ;
		gaussFilter(1.4, input, inputWithoutNoise) ; // reduction du bruit
		final IntervalView<UnsignedByteType> expandedView = Views.expandZero(inputWithoutNoise, 1, 1) ; // the size of the filter (1) ; expanding on zero to detect the contours of the img
		final RandomAccess<UnsignedByteType> rInput = expandedView.randomAccess(), rIout = output.randomAccess() ;
		int [] [] gradientNorma = new int [(int) inputWithoutNoise.max(0)+1][(int) inputWithoutNoise.max(1)+1] ;
		double [] [] gradientAngle = new double [(int) inputWithoutNoise.max(0)+1][(int) inputWithoutNoise.max(1)+1] ;
	
		for(int x = 0; x <= inputWithoutNoise.max(0) ; x++){
			for(int y = 0; y <= inputWithoutNoise.max(1) ; y++){
				int sumX = 0, sumY = 0 ;
				for(int i = -1 ; i <= 1; i++){
					rInput.setPosition(x+i, 0);
					rInput.setPosition(y, 1);
					sumX+= rInput.get().get() * i ;
					rInput.setPosition(x, 0);
					rInput.setPosition(y+i, 1);
					sumY+= rInput.get().get() * i ;
				}
				gradientNorma[x][y] =  (int) Math.round(Math.sqrt(sumX*sumX + sumY*sumY)) ;
				double atan = Math.atan2(sumY, sumX) ;
				gradientAngle[x][y] =  (Math.toDegrees(atan) + 180.0) % 180.0 ;
			}
		}
		int highTreshold = 10, lowTreshold = highTreshold/2 ;
	
		for(int x = 0; x <= inputWithoutNoise.max(0) ; x++){
			for(int y = 0; y <= inputWithoutNoise.max(1) ; y++){
				rIout.setPosition(x, 0);
				rIout.setPosition(y, 1);
				int dx = 0, dy = 0 ; // dx et dy contiennent les valeurs a ajouter respectivement a x et y pour avoir les
				// voisins du point : x+dx, y+dy ; x-dx , y-dy
				if (gradientAngle[x][y] <= 22.5){
					dx = 1 ; // gradient horizontal
				}
				else if (gradientAngle[x][y] <= 77.5){
					dx = 1 ; dy = -1 ; // coin droit haut
				}
				else if (gradientAngle[x][y] <= 112.5){
						dy = -1 ; // gradient vertical
				}
				else if (gradientAngle[x][y] <= 157.5){
					dx = -1 ; dy = -1 ; // coin gauche haut
				}
				else {
					dx = 1 ; // gradient horizontal
				}
				// On doit mettre la bonne valeur ou il faut
				int valuexyz = gradientNorma[x][y] ;
				if (x+dx >= 0 && x+dx <= inputWithoutNoise.max(0) && y+dy >= 0 && y+dy <= inputWithoutNoise.max(1) && gradientNorma[x+dx][y+dy] > gradientNorma[x][y]){
					valuexyz = 0 ;
				}
				if (x-dx >= 0 && x-dx <= inputWithoutNoise.max(0) && y-dy >= 0 && y-dy <= inputWithoutNoise.max(1) && gradientNorma[x-dx][y-dy] > gradientNorma[x][y]){
					valuexyz = 0 ;
				}
				// maintenant on fait le premier seuillage de l'image
				if (valuexyz < lowTreshold) valuexyz = 0 ;
				if (valuexyz >= highTreshold) valuexyz = 255;
				rIout.get().set(valuexyz) ;
			}
		}
	
		// on va maintenant s'occuper des pixels entre lowTreshold et highTreshold
	
		for(int x = 0; x <= inputWithoutNoise.max(0) ; x++){
			for(int y = 0; y <= inputWithoutNoise.max(1) ; y++){
				rIout.setPosition(x, 0);
				rIout.setPosition(y, 1);
				int dx = 0, dy = 0 ; // dx et dy contiennent les valeurs a ajouter respectivement a x et y pour avoir les
				// voisins du point : x+dx, y+dy ; x-dx , y-dy
				if (gradientAngle[x][y] <= 22.5){
					dx = 1 ; // gradient horizontal
				}
				else if (gradientAngle[x][y] <= 77.5){
					dx = 1 ; dy = -1 ; // coin droit haut
				}
				else if (gradientAngle[x][y] <= 112.5){
						dy = -1 ; // gradient vertical
				}
				else if (gradientAngle[x][y] <= 157.5){
					dx = -1 ; dy = -1 ; // coin gauche haut
				}
				else {
					dx = 1 ; // gradient horizontal
				}
				int valuexyz = 0 ;
				if (gradientNorma[x][y] >= lowTreshold && gradientNorma[x][y] < highTreshold){
					if (x+dx >= 0 && x+dx <= inputWithoutNoise.max(0) && y+dy >= 0 && y+dy <= inputWithoutNoise.max(1) && gradientNorma[x+dx][y+dy] == 255){
						// point deja accepte
						valuexyz = 255 ;
					}
					if (x-dx >= 0 && x-dx <= inputWithoutNoise.max(0) && y-dy >= 0 && y-dy <= inputWithoutNoise.max(1) && gradientNorma[x-dx][y-dy] == 255){
						valuexyz = 255 ;
					}
					rIout.get().set(valuexyz) ;
				}
			}
		}
	}
	/**
	 * Apply a Canny filter (contour filter) on a given colored image
	 * @param input the given colored image
	 * @param output the result of the filter
	 */
    public static void cannyFilterColor(final Img<UnsignedByteType> input, final Img<UnsignedByteType> output, boolean isGrayLevel){
		if (isGrayLevel) ColorUtilitariesProcessing.colorToGrayLBMultithreaded(input);
		final Img<UnsignedByteType> inputWithoutNoise = input.copy() ;
		gaussFilter(1.4, input, inputWithoutNoise) ; // reduction du bruit

		final IntervalView<UnsignedByteType> expandedView = Views.expandZero(inputWithoutNoise, 1, 1, 0) ; // the size of the filter (1) ; expanding on zero to detect the contours of the img
		
        final RandomAccess<UnsignedByteType> rInput = expandedView.randomAccess(), rIout = output.randomAccess() ;

        int [] [] [] gradientNorma = new int [(int) inputWithoutNoise.max(0)+1][(int) inputWithoutNoise.max(1)+1][(int) inputWithoutNoise.max(2)+1] ;
         double [] [] [] gradientAngle = new double [(int) inputWithoutNoise.max(0)+1][(int) inputWithoutNoise.max(1)+1][(int) inputWithoutNoise.max(2)+1] ;

        for(int x = 0; x <= inputWithoutNoise.max(0) ; x++){
            for(int y = 0; y <= inputWithoutNoise.max(1) ; y++){
                for (int z = 0 ; z <= inputWithoutNoise.max(2) ; z++){
                    int sumX = 0, sumY = 0 ;
                    rInput.setPosition(z,2);
                    for(int i = -1 ; i <= 1; i++){
                        rInput.setPosition(x+i, 0);
                        rInput.setPosition(y, 1);
                        sumX+= rInput.get().get() * i ;
                        rInput.setPosition(x, 0);
                        rInput.setPosition(y+i, 1);
                        sumY+= rInput.get().get() * i ;
                    }
                    gradientNorma[x][y][z] =  (int) Math.round(Math.sqrt(sumX*sumX + sumY*sumY)) ;
                    double atan = Math.atan2(sumY, sumX) ;
                    gradientAngle[x][y][z] =  (Math.toDegrees(atan) + 180.0) % 180.0 ;
                }
            }
        }
       int highTreshold = 10, lowTreshold = highTreshold/2 ;

        for(int x = 0; x <= inputWithoutNoise.max(0) ; x++){
            for(int y = 0; y <= inputWithoutNoise.max(1) ; y++){
                for (int z = 0 ; z <= inputWithoutNoise.max(2) ; z++){
                    // on met la valeur dans interOutput
                    rIout.setPosition(x, 0);
                    rIout.setPosition(y, 1);
                    rIout.setPosition(z, 2);
                    int dx = 0, dy = 0 ; // dx et dy contiennent les valeurs a ajouter respectivement a x et y pour avoir les
                    // voisins du point : x+dx, y+dy ; x-dx , y-dy
                    if (gradientAngle[x][y][z] <= 22.5){
                        dx = 1 ; // gradient horizontal
                    }
                    else if (gradientAngle[x][y][z] <= 77.5){
                        dx = 1 ; dy = -1 ; // coin droit haut
                    }
                    else if (gradientAngle[x][y][z] <= 112.5){
                         dy = -1 ; // gradient vertical
                    }
                    else if (gradientAngle[x][y][z] <= 157.5){
                        dx = -1 ; dy = -1 ; // coin gauche haut
                    }
                    else {
                        dx = 1 ; // gradient horizontal
                    }
                    // On doit mettre la bonne valeur ou il faut
                    int valuexyz = gradientNorma[x][y][z] ;
                    if (x+dx >= 0 && x+dx <= inputWithoutNoise.max(0) && y+dy >= 0 && y+dy <= inputWithoutNoise.max(1) && gradientNorma[x+dx][y+dy][z] > gradientNorma[x][y][z]){
                        valuexyz = 0 ;
                    }
                    if (x-dx >= 0 && x-dx <= inputWithoutNoise.max(0) && y-dy >= 0 && y-dy <= inputWithoutNoise.max(1) && gradientNorma[x-dx][y-dy][z] > gradientNorma[x][y][z]){
                        valuexyz = 0 ;
                    }
                    // maintenant on fait le premier seuillage de l'image
                    if (valuexyz < lowTreshold) valuexyz = 0 ;
                    if (valuexyz >= highTreshold) valuexyz = 255;
                    rIout.get().set(valuexyz) ;
                }
            }
        }

        // on va maintenant s'occuper des pixels entre lowTreshold et highTreshold

        for(int x = 0; x <= inputWithoutNoise.max(0) ; x++){
            for(int y = 0; y <= inputWithoutNoise.max(1) ; y++){
                for (int z = 0 ; z <= inputWithoutNoise.max(2) ; z++){
                    // on met la valeur dans interOutput
                    rIout.setPosition(x, 0);
                    rIout.setPosition(y, 1);
                    rIout.setPosition(z, 2);
                    int dx = 0, dy = 0 ; // dx et dy contiennent les valeurs a ajouter respectivement a x et y pour avoir les
                    // voisins du point : x+dx, y+dy ; x-dx , y-dy
                    if (gradientAngle[x][y][z] <= 22.5){
                        dx = 1 ; // gradient horizontal
                    }
                    else if (gradientAngle[x][y][z] <= 77.5){
                        dx = 1 ; dy = -1 ; // coin droit haut
                    }
                    else if (gradientAngle[x][y][z] <= 112.5){
                         dy = -1 ; // gradient vertical
                    }
                    else if (gradientAngle[x][y][z] <= 157.5){
                        dx = -1 ; dy = -1 ; // coin gauche haut
                    }
                    else {
                        dx = 1 ; // gradient horizontal
                    }
                    int valuexyz = 0 ;
                    if (gradientNorma[x][y][z] >= lowTreshold && gradientNorma[x][y][z] < highTreshold){
                        if (x+dx >= 0 && x+dx <= inputWithoutNoise.max(0) && y+dy >= 0 && y+dy <= inputWithoutNoise.max(1) && gradientNorma[x+dx][y+dy][z] == 255){
                            // point deja accepte
                            valuexyz = 255 ;
                        }
                        if (x-dx >= 0 && x-dx <= inputWithoutNoise.max(0) && y-dy >= 0 && y-dy <= inputWithoutNoise.max(1) && gradientNorma[x-dx][y-dy][z] == 255){
                            valuexyz = 255 ;
                        }
                        rIout.get().set(valuexyz) ;
                    }
                }
            }
        }
	}

	/**
	 * Apply a gaussian filter on a given image for a given sigma value. The image is expanded by single mirror
	 * Calls imglib2 algorithm
	 * @param value the sigma value of the gaussian filter
	 * @param input the given image
	 * @param output the result of the filter
	 */
	public static void gaussFilter(double value, final Img<UnsignedByteType> input, final Img<UnsignedByteType> output) {
		// The size of the kernel is 2*3*value +1, we have to expend the value size.
		int filterSize = Math.max( 2, ( int ) ( 3 * value + 0.5 ) + 1 ) ;
		IntervalView<UnsignedByteType> expandedView ;
		if (input.numDimensions() == 3)
			expandedView = Views.expandMirrorSingle(input, filterSize, filterSize, 0) ;
		else
			 expandedView = Views.expandMirrorSingle(input, filterSize, filterSize) ;


		Interval interval = Intervals.expand(expandedView, - filterSize) ; // we suppress the pixels which are outside
		final IntervalView<UnsignedByteType> view = Views.interval(expandedView, interval) ; //we reduct the view at the real size of the img

		if (input.numDimensions() == 3)
			Gauss3.gauss(new double [] {value, value, 0}, view, output) ;
		else
			Gauss3.gauss(new double [] {value, value}, view, output) ;
	}
}
