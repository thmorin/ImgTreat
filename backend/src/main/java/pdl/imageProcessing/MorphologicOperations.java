package pdl.imageProcessing;

import java.util.function.BiFunction;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;

/**
 * This class stores some methods about the morphologics operations on an image
 */
public class MorphologicOperations {
    /**
     * Apply a generic morphologic operation by applying a filter in cross and reduce it by functionToApply
     */
    private static void genericMorphologicOperationSize3(Img<UnsignedByteType> input, Img<UnsignedByteType> output, BiFunction<Integer, Integer, Integer> functionToApply, int startValue){
        Cursor<UnsignedByteType> cOutput = output.cursor() ;
        RandomAccess<UnsignedByteType> rInput = input.randomAccess() ;
        boolean isGrayLevel = input.numDimensions() == 3 ;
        while(cOutput.hasNext()){
            cOutput.fwd() ;
            int x = cOutput.getIntPosition(0), y = cOutput.getIntPosition(1) ;
            if (isGrayLevel)
                rInput.setPosition(cOutput.getIntPosition(2), 2);
            int value = startValue ;
            for(int dx = -1; dx <= 1; dx++){
                if (x+dx < 0 || x+dx > input.max(0))
                    continue ;
                rInput.setPosition(x+dx, 0);
                for(int dy = -1; dy <= 1; dy++){
                    if (y+dy >= 0 && y+dy <= input.max(1) && Math.abs(dx) + Math.abs(dy) != 2){
                        // On ne regarde pas en -1,-1 ; -1, 1 ; 1,-1 ; 1,1
                        rInput.setPosition(y+dy, 1) ;
                        value = functionToApply.apply(value, rInput.get().get()) ;
                    }
                }
            }
            cOutput.get().set(value);
        }
    }
    /**
     * Apply an erosion of an image
     * @param input the image to erode
     * @param output the result of the erosion
     */
    public static void erosion(Img<UnsignedByteType> input, Img<UnsignedByteType> output){
        genericMorphologicOperationSize3(input, output, (x, y) -> Math.min(x, y), 256);
    }
    /**
     * Apply a dilatation of an image
     * @param input the image to dilatate
     * @param output the result of the dilatation
     */
    public static void dilatation(Img<UnsignedByteType> input, Img<UnsignedByteType> output){
        genericMorphologicOperationSize3(input, output, (x, y) -> Math.max(x, y), -1);
    }
}