package pdl.imageProcessing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;


class InfPixel {
    public int val=0;
    public int pred=0;
    
    public InfPixel(int val,int pred){
        this.val=val;
        this.pred=pred;
    }
    public void setVal(int val){
        this.val=val;
    }
}

public class SeamCarving {


	/**
	 * This class is a simple way to store pair of key, value
	 */
	private static class Pair<V>{
		private int key ;
		private V value ;
		Pair(int k, V v){
			this.key = k ; this.value = v ;
		}
		int getKey(){
			return key ;
		}
		void setKey(int key){
			this.key = key ;
		}
		void incKey(){
			this.key++ ;
		}
		void decKey(){
			this.key-- ;
		}
		V getValue(){
			return value ;
		}
		void setValue(V value){
			this.value = value ;
		}
	}
	/**
	 * This function apply a really simplified version of the seam carving algorithm
	 * We don't take the seam, but we take vertical or horizontal of low energy
	 */
	public static void simplifiedSeamCarving(Img<UnsignedByteType> input, Img<UnsignedByteType> output, long newWidth, long newHeight){
		long oldWidth = input.max(0), oldHeight = input.max(1) ;

		if (newWidth < 0 || newHeight < 0 || newWidth > 2*(oldWidth+1) || newHeight > 2*(oldHeight+1) ){
			throw new RuntimeException("la nouvelle taille est incompatible avec l'image") ;
		}
		Img<UnsignedByteType> inputCp = input.copy(), outputCp = input.copy() ;
		if (input.numDimensions() == 3)
			Convolution.cannyFilterColor(inputCp, outputCp, true);
		else
			Convolution.cannyFilterGrayLevel(inputCp, outputCp);
		// On commence par computer les pixels de width que l'on garde pas
		List<Pair<Integer>> columnWeight = new ArrayList<>((int) oldWidth) ;
		RandomAccess<UnsignedByteType> rInputCanny = outputCp.randomAccess() ;
		rInputCanny.setPosition(0, 1);
		for(int x = 0 ; x < oldWidth ; x++){
			rInputCanny.setPosition(x, 0); // on a pas besoin de parcourir le z : toutes les slices on le meme poids : canny en niveaux de gris
			columnWeight.add(new Pair<>(x, rInputCanny.get().get())) ;
		}
		for(int y = 1 ; y < oldHeight ; y++){
			rInputCanny.setPosition(y, 1);
			for(int x = 0 ; x < oldWidth ; x++){
				rInputCanny.setPosition(x, 0);
				columnWeight.get(x).setValue(columnWeight.get(x).getValue() + rInputCanny.get().get());
			}
		}
		// On a dans columnWeight une liste de paire de valeur tq la pair X, Y correspond a "le poids de la colonne X est Y"

		Collections.sort(columnWeight, new Comparator<Pair<Integer>>(){
			@Override
			public int compare(Pair<Integer> p1, Pair<Integer> p2) {
				return p1.getValue() - p2.getValue() ;
			}
		} );
		// on a maintenant dans columnWeight une liste triée de paire de valeur
		long toSupressWidth = oldWidth - newWidth, toAddWidth = -toSupressWidth ; // toSupressWidth contient le nombre de colonnes a supprimer

		ArrayList<Pair<Boolean>> arrayPosWidth = new ArrayList<>((int) oldWidth) ; 
		for(int i = 0 ; i < oldWidth ; i++) arrayPosWidth.add(new Pair<>(i, false)) ; // l'idee est que arrayPosWidth[i] contienne le nouvel indice de la ligne i (c'est la clé de la pair)
		// et la value contienne si il faut doubler ou non le pixel (false pour non, true pour oui) dans la nouvelle image
		for(int x = 0 ; x < toSupressWidth ; x++){ // cas de suppression de colonnes
			Pair<Integer> pairToSupress = columnWeight.get(x) ; // pairToSupress contient la colonne a supprimer
			arrayPosWidth.get(pairToSupress.getKey()).setKey(-1) ; // On indique que la colonne n'est pas a mettre
			for(int xm = pairToSupress.getKey()+1 ; xm < oldWidth ; xm++){
				arrayPosWidth.get(xm).decKey() ; // on decale toutes les colonnes a la droite de la valeur a supprimer
			}
		}
		for(int x = 0 ; x < toAddWidth ; x++){ //cas de supression de lignes
			Pair<Integer> pairToAdd = columnWeight.get(x) ; // l'idee est que on met cette colonne a sa place et a sa droite et donc on doit decaler de 1 toutes les colonnes de a droite
			arrayPosWidth.get(pairToAdd.getKey()).setValue(true) ; // On indique que la colonne est a doubler
			for(int xm = pairToAdd.getKey()+1 ; xm < oldWidth ; xm++){
				arrayPosWidth.get(xm).incKey(); // on decale toutes les colonnes a la droite de la valeur a ajouter
			}
		}

		// On refait la meme chose mais dans le sens de la hauteur

		List<Pair<Integer>> rowWeight = new ArrayList<>((int) oldHeight) ;
		rInputCanny.setPosition(0, 0);
		for(int y = 0 ; y < oldHeight ; y++){
			rInputCanny.setPosition(y, 1); // on a pas besoin de parcourir le z : toutes les slices on le meme poids : canny en niveaux de gris
			rowWeight.add(new Pair<>(y, rInputCanny.get().get())) ;
		}
		for(int x = 1 ; x < oldWidth ; x++){
			rInputCanny.setPosition(x, 0);
			for(int y = 0 ; y < oldHeight ; y++){
				rInputCanny.setPosition(y, 1);
				rowWeight.get(y).setValue(rowWeight.get(y).getValue() + rInputCanny.get().get());
			}
		}
		// On a dans columnWeight une liste de paire de valeur tq la pair X, Y correspond a "le poids de la colonne X est Y"

		Collections.sort(rowWeight, new Comparator<Pair<Integer>>(){
			@Override
			public int compare(Pair<Integer> p1, Pair<Integer> p2) {
				return p1.getValue() - p2.getValue() ;
			}
		} );
		// on a maintenant dans columnWeight une liste triée de paire de valeur
		long toSupressHeight = oldHeight - newHeight, toAddHeight = -toSupressHeight ; // toSupress contient le nombre de valeur a supprimer

		ArrayList<Pair<Boolean>> arrayPosHeight = new ArrayList<>((int) oldHeight) ; 
		for(int i = 0 ; i < oldHeight ; i++) arrayPosHeight.add(new Pair<>(i, false)) ; // l'idee est que la cle de arrayPosHeight[i] contienne la 
		// nouvelle position de la ligne i et la value dise si il faut doubler la ligne ou non
		for(int y = 0 ; y < toSupressHeight ; y++){
			Pair<Integer> pairToSupress = rowWeight.get(y) ; // pairToSupress contient la colonne a supprimer
			arrayPosHeight.get(pairToSupress.getKey()).setKey(-1) ; // On indique que la colonne n'est pas a mettre
			for(int ym = pairToSupress.getKey()+1 ; ym < oldHeight ; ym++){
				arrayPosHeight.get(ym).decKey();
			}
		}

		for(int y = 0 ; y < toAddHeight ; y++){ //cas d'ajout de lignes
			Pair<Integer> pairToAdd = rowWeight.get(y) ; // l'idee est que on met cette ligne a sa place et a sa droite et donc on doit decaler de 1 toutes les lignes de a droite
			arrayPosHeight.get(pairToAdd.getKey()).setValue(true) ; // On indique que la ligne est a doubler
			for(int ym = pairToAdd.getKey()+1 ; ym < oldHeight ; ym++){
				arrayPosHeight.get(ym).incKey() ; // on decale toutes les colonnes a la droite de la valeur a ajouter
			}
		}


		// maintenant il reste juste a parcourir arrayPos et d'injecter les colonnes dans la nouvelle image
		RandomAccess<UnsignedByteType> rInput = input.randomAccess(), rOutput = output.randomAccess() ;

		for(int x = 0 ; x < oldWidth ; x++){
			if(arrayPosWidth.get(x).getKey() >= 0) { // on met la colonne correspondante de rInput a rOutput
				rInput.setPosition(x, 0) ;
				for(int i = 0 ; i < (arrayPosWidth.get(x).getValue() ? 2 : 1) ; i++){
					rOutput.setPosition(arrayPosWidth.get(x).getKey() + i, 0) ;
					for(int y = 0 ; y < oldHeight ; y++){
						if (arrayPosHeight.get(y).getKey() >= 0){ //On met la ligne correspondante de rInput a rOutput
							rInput.setPosition(y, 1) ;
							for(int j = 0 ; j < (arrayPosHeight.get(y).getValue() ? 2 : 1) ; j++){
								rOutput.setPosition(arrayPosHeight.get(y).getKey() +j, 1) ;
								if (input.numDimensions() == 3){
									// image coloree
									for(int z = 0; z < 3 ; z++){
										rOutput.setPosition(z, 2); rInput.setPosition(z, 2);
										rOutput.get().set(rInput.get().get()) ;
									}
								}
								else { // image GL
									rOutput.get().set(rInput.get().get()) ;
								}
							}
						}
					}
				}
			}
		}
	}

    /**
	 * This function is used to return a list of the positions of the seam to delete on the
	 * algorithm of seam carving
	 * @param tab a matrix of paths
	 * @param minPath the indice of the lower path on tab
	 */
	private static List<Integer> positionsOfPathWidth(Img<UnsignedByteType> input, InfPixel[][] paths,int minPath){
        final int ih = (int) input.max(1);
		List<Integer> path= new ArrayList<Integer>();
		int x = minPath;
		path.add(x);
		for(int y=0;y<ih;y++){//simple boucle pour remonter tout les prédécesseur
			path.add(paths[x][y].pred);
			x=paths[x][y].pred;
		}
		return path;
	}

	/**
	 * Delete the given path on a given image and shift all the others pixels if necessary
	 * 
	 * @param input the given image
	 * @param pos the path to delete
	 * @param mawW the new width of the image
	 */
	private static void shiftPositionWidth(Img<UnsignedByteType> input, List<Integer> pos,int maxW){
		final RandomAccess<UnsignedByteType> r = input.randomAccess();
		int y=0;
		
		for(int i=0;i<pos.size();i++ ){//parcour la liste de position
			r.setPosition(y, 1);
			for(int x=pos.get(i);x<maxW;x++){//se place à la position à supprimer 
				if(input.numDimensions()==3){
					for(int z=0; z<=2;z++){//image en couleur
						r.setPosition(x+1, 0);
						r.setPosition(z,2);
						int val = r.get().get();
						r.setPosition(x, 0);
						r.get().set(val);//décale les positions

					}
				}
				else{
					r.setPosition(x+1,0);
					int val = r.get().get();
					r.setPosition(x, 0);
					r.get().set(val);//décale les positions
				}
			}
			y++;
		}
		
		
	}
	private static void shiftPositionWidthIncrease(Img<UnsignedByteType> input, List<Integer> pos,int maxW){
		final RandomAccess<UnsignedByteType> r = input.randomAccess();
		int y=0;
		for(int i=0;i<pos.size();i++ ){//parcour la liste de position 
			r.setPosition(y, 1);
			for(int x=maxW;x>pos.get(i);x--){//se place en bordure, décale l'image, et double le pixel en position x
				if(input.numDimensions()==3){//image en couleur
					for(int z=0; z<=2;z++){
						r.setPosition(x-1, 0);
						r.setPosition(z,2);
						int val = r.get().get();
						r.setPosition(x, 0);
						r.get().set(val);//
	
					}
				}
				else{
					r.setPosition(x-1,0);
					int val = r.get().get();
					r.setPosition(x, 0);
					r.get().set(val);
				}
			}
			y++;
		}
		
	}

	
	/**
	 * Return the indice of the begin of the best path
	 * 
	 * @param input the given image
	 * @param paths a matrix of paths
	 * @param mawW the number of paths to consider
	 */
	private static int bestPathWidth(InfPixel[][] paths,int maxW,boolean decrease){
		int best = paths[0][0].val ;
		int bestPath = 0;
		if(decrease){
			for(int x=1;x<=maxW;x++){
				if(paths[x][0].val<best){
					best = paths[x][0].val;
					bestPath = x;
				}
			}
		}
		/*regarde le plus petit ou le plus grand de la ligne*/
		else{
			for(int x=1;x<=maxW;x++){
				if(paths[x][0].val>best){
					best = paths[x][0].val;
					bestPath = x;
				}
			}
		}
		return bestPath;
	}


	/**
	 * Return a matrix of paths, defined by an array of array of InfPixel
	 * 
	 * @param input the given image
	 * @param mawW the width to consider of the image
	 */
	private static InfPixel[][] computePathsWidth(Img<UnsignedByteType> input,int maxW,boolean decrease){
		final RandomAccess<UnsignedByteType> r = input.randomAccess();
        final int iw = (int) input.max(0);
        final int ih = (int) input.max(1);
		InfPixel[][] val = new InfPixel[iw+1][ih+1];
		for(int y=0 ; y<=ih;y++){
			for(int x=0 ; x<=maxW;x++){
				if(y==ih){
					r.setPosition(x, 0);
					r.setPosition(y, 1);
					val[x][y]=new InfPixel(r.get().get(), 0);//sur les cases de la derniere rangée, met les valeur du pixel
				}
				else{
					val[x][y]=new InfPixel(255*ih, 0);//met la valeur maximum dans les autres cases
				}
			}
		}
		int g,m,d;
		
		if(decrease){//recherche du chemin le plus faible

			for(int y=ih;y>0;y--){//on part du bas de l'image et on remonte 
				for(int x=0;x<=maxW;x++){//on parcours l'image de gauche à droite
					if(x==0){//condition pour ne pas dépasser les bords
						r.setPosition(x, 0);
						r.setPosition(y-1, 1);
						m=r.get().get();
						r.setPosition(x+1, 0);
						r.setPosition(y-1, 1);
						d=r.get().get();
						if(m+val[x][y].val<val[x][y-1].val){
							val[x][y-1].val=m+val[x][y].val;
							val[x][y-1].pred=x;
						}
						if(d+val[x+1][y].val<val[x+1][y-1].val){
							val[x+1][y-1].val=d+val[x][y].val;
							val[x+1][y-1].pred=x;
						}
					}
					else if(x==maxW){//condition pour ne pas dépasser les bords
						r.setPosition(x, 0);
						r.setPosition(y-1, 1);
						m=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y-1, 1);
						g=r.get().get();
						if(m+val[x][y].val<val[x][y-1].val){
							val[x][y-1].val=m+val[x][y].val;
							val[x][y-1].pred=x;
						}
						if(g+val[x][y].val<val[x-1][y-1].val){
							val[x-1][y-1].val=g+val[x][y].val;
							val[x-1][y-1].pred=x;
						}
					}
					else{
						r.setPosition(x, 0);
						r.setPosition(y-1, 1);
						m=r.get().get();//prends la valeur un pixel au dessus
						r.setPosition(x-1, 0);
						r.setPosition(y-1, 1);
						g=r.get().get();//prends la valeur un pixel au dessus, un pixel à gauche
						r.setPosition(x+1, 0);
						r.setPosition(y-1, 1);
						d=r.get().get();//prends la valeur un pixel au dessus, un pixel à droite 
						if(m+val[x][y].val<val[x][y-1].val){//regarde si la case milieu au dessus est remplaçable par une valeur plus petite
							val[x][y-1].val=m+val[x][y].val;//remplace la valeur
							val[x][y-1].pred=x;//inscrit le chemin pour pouvoir le remonter
						}
						if(g+val[x][y].val<val[x-1][y-1].val){//regarde si la case gauche au dessus est remplaçable par une valeur plus petite
							val[x-1][y-1].val=g+val[x][y].val;//remplace la valeur
							val[x-1][y-1].pred=x;//inscrit le chemin pour pouvoir le remonter
						}
						if(d+val[x+1][y].val<val[x+1][y-1].val){//regarde si la case droite au dessus est remplaçable par une valeur plus petite
							val[x+1][y-1].val=d+val[x][y].val;//remplace la valeur
							val[x+1][y-1].pred=x;//inscrit le chemin pour pouvoir le remonter
						}
					}
				}
			}
		}
		/*Cette partie est identique à celle au dessus,
		*à la différence qu'on cherche le chemin le
		*plus fort, donc on inverse les conditions*/
		else{
			for(int y=ih;y>0;y--){
				for(int x=0;x<=maxW;x++){
					if(x==0){
						r.setPosition(x, 0);
						r.setPosition(y-1, 1);
						m=r.get().get();
						r.setPosition(x+1, 0);
						r.setPosition(y-1, 1);
						d=r.get().get();
						if(m+val[x][y].val>val[x][y-1].val){
							val[x][y-1].val=m+val[x][y].val;
							val[x][y-1].pred=x;
						}
						if(d+val[x+1][y].val>val[x+1][y-1].val){
							val[x+1][y-1].val=d+val[x][y].val;
							val[x+1][y-1].pred=x;
						}
					}
					else if(x==maxW){
						r.setPosition(x, 0);
						r.setPosition(y-1, 1);
						m=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y-1, 1);
						g=r.get().get();
						
						if(m+val[x][y].val>val[x][y-1].val){
							val[x][y-1].val=m+val[x][y].val;
							val[x][y-1].pred=x;
						}
						if(g+val[x][y].val>val[x-1][y-1].val){
							val[x-1][y-1].val=g+val[x][y].val;
							val[x-1][y-1].pred=x;
						}
						
					}
					else{
						r.setPosition(x, 0);
						r.setPosition(y-1, 1);
						m=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y-1, 1);
						g=r.get().get();
						r.setPosition(x+1, 0);
						r.setPosition(y-1, 1);
						d=r.get().get();
						if(m+val[x][y].val>val[x][y-1].val){
							val[x][y-1].val=m+val[x][y].val;
							val[x][y-1].pred=x;
						}
						if(g+val[x][y].val>val[x-1][y-1].val){
							val[x-1][y-1].val=g+val[x][y].val;
							val[x-1][y-1].pred=x;
						}
						if(d+val[x+1][y].val>val[x+1][y-1].val){
							val[x+1][y-1].val=d+val[x][y].val;
							val[x+1][y-1].pred=x;
						}
					}
				}
			}
		}
		return val;
	}

	
	/**
	 * This algorithm is a greedy simplified version of the seam carving
	 * We suppress the sup lower seams of the image
	 * @param input the given image
	 * @param sup number of pixel to reduce
	 */
	public static void greedySeamCarvingWidth(Img<UnsignedByteType> input,Img<UnsignedByteType> output, int sup,boolean	decrease){
		final RandomAccess<UnsignedByteType> r = input.randomAccess();
        final int iw = (int) input.max(0);
        final int ih = (int) input.max(1);
		Img<UnsignedByteType> canny;
		
		final RandomAccess<UnsignedByteType> res = output.randomAccess();
		if(!decrease){//si on augmente l'image

			/*On copie ce qu'il y a dans image vers output
			*car on travaillera sur output qui à les bonnes 
			*dimensions, ce qui nous évitera des Out of Bounds */
			for(int x=0;x<=iw;x++){
				for(int y=0;y<=ih;y++){
					r.setPosition(y, 1);
					res.setPosition(y, 1);
					if(output.numDimensions()==3){

						for(int z=0; z<=2;z++){
							r.setPosition(x, 0);
							res.setPosition(x, 0);
							r.setPosition(z,2);
							res.setPosition(z,2);
							res.get().set(r.get().get());
						}
					}
					else{
						r.setPosition(x,0);
						res.setPosition(x, 0);
						res.get().set(r.get().get());
					}
				}
			}
			canny = output.copy();
			/*Le filtre de canny permet de mieux décerner 
			*les contours, or c'est ce qui nous intéresse le
			*plus pour supprimer les éléments "moins inutiles"
			*de l'image*/
			if(output.numDimensions()==3){//si l'image est en couleur
				Img<UnsignedByteType> outputCp = output.copy() ;
				Convolution.cannyFilterColor(outputCp, canny, true);//filtre de contour sur canny à partir de output initialisé plus haut
			}
			else{
				Convolution.cannyFilterGrayLevel(output, canny);//filtre de contour sur canny à partir de output initialisé plus haut
			}
		}
		else{
			canny = input.copy();
			if(input.numDimensions()==3){
				Img<UnsignedByteType> inputCp = input.copy() ;
				Convolution.cannyFilterColor(inputCp, canny, true);//filtre de contour sur canny à partir de output initialisé plus haut
			}
			else{
				Convolution.cannyFilterGrayLevel(input, canny);//filtre de contour sur canny à partir de output initialisé plus haut
			}
		}

		

		for(int x=0;x<sup;x++){//un tour de boucle pour chaque chemin à supprimer
			int maxW;//maxW est la nouvelle largeur d'image
			if(decrease){maxW = iw-x;}//si on diminue l'image
			else{maxW = iw+x;}//si on l'augmente 
			
			InfPixel[][] paths = new InfPixel[maxW+1][ih+1];//création du tableau de InfPixel
			
			paths = computePathsWidth(canny, maxW,decrease);//Calcule du tableau
			
			int bestPath = bestPathWidth(paths, maxW,decrease);//recherche du meilleur chemin de ce tableau
			
			List<Integer> positionsBestPath ;
			positionsBestPath = positionsOfPathWidth(canny, paths, bestPath);//Le meilleur chemin
			
			if(!decrease){
				/*Même si on augmente la taille de l'image,
				* on doit supprimer le chemin trouvé pour
				* ne pas retomber dessus*/
				shiftPositionWidthIncrease(output, positionsBestPath, maxW);
				shiftPositionWidth(canny, positionsBestPath,maxW);
			}
			else{
				shiftPositionWidth(input, positionsBestPath,maxW);
				shiftPositionWidth(canny, positionsBestPath,maxW);
			}
			
			

		}
		
		if(decrease){//on met les chanement dans la nouvelle image
			for(int x=0;x<=iw-sup;x++){
				for(int y=0;y<=ih;y++){
					r.setPosition(y, 1);
					res.setPosition(y, 1);
					if(output.numDimensions()==3){
						for(int z=0; z<=2;z++){
							r.setPosition(x, 0);
							res.setPosition(x, 0);
							r.setPosition(z,2);
							res.setPosition(z,2);
							res.get().set(r.get().get());
						}
					}
					else{
						r.setPosition(x,0);
						res.setPosition(x, 0);
						res.get().set(r.get().get());
					}
				}
			}
		}
		
		
	}





	/* seamCarvingHeight fonctionne d'exactement la même facon
	* que seamCarvingWidth, on inverse juste le sens de 
	* parcour de l'image */




	/**
	 * This function is used to return a list of the positions of the seam to delete on the
	 * algorithm of seam carving
	 * @param tab a matrix of paths
	 * @param minPath the indice of the lower path on tab
	 */
	private static List<Integer> positionsOfPathHeight(Img<UnsignedByteType> input, InfPixel[][] paths,int minPath){
        final int iw = (int) input.max(0);
		List<Integer> path= new ArrayList<Integer>();
		int y = minPath;
		path.add(y);
		for(int x=0;x<iw;x++){
			path.add(paths[x][y].pred);
			y=paths[x][y].pred;
		}
		return path;
	}

	/**
	 * Delete the given path on a given image and shift all the others pixels if necessary
	 * 
	 * @param input the given image
	 * @param pos the path to delete
	 * @param mawW the new width of the image
	 */
	private static void shiftPositionHeight(Img<UnsignedByteType> input, List<Integer> pos,int maxH){
		final RandomAccess<UnsignedByteType> r = input.randomAccess();
		int x=0;
		
		for(int i=0;i<pos.size();i++ ){
			r.setPosition(x, 0);
			for(int y=pos.get(i);y<maxH;y++){
				if(input.numDimensions()==3){
					for(int z=0; z<=2;z++){
						r.setPosition(y+1, 1);
						r.setPosition(z,2);
						int val = r.get().get();
						r.setPosition(y, 1);
						r.get().set(val);

					}
				}
				else{
					r.setPosition(y+1,1);
					int val = r.get().get();
					r.setPosition(y,1);
					r.get().set(val);
				}
			}
			x++;
		}
		
		
	}
	private static void shiftPositionHeightIncrease(Img<UnsignedByteType> input, List<Integer> pos,int maxH){
		final RandomAccess<UnsignedByteType> r = input.randomAccess();
		int x=0;
		for(int i=0;i<pos.size();i++ ){
			r.setPosition(x, 0);
			for(int y=maxH;y>pos.get(i);y--){
				if(input.numDimensions()==3){
					for(int z=0; z<=2;z++){
						r.setPosition(y-1, 1);
						r.setPosition(z,2);
						int val = r.get().get();
						r.setPosition(y, 1);
						r.get().set(val);
	
					}
				}
				else{
					r.setPosition(y-1,1);
					int val = r.get().get();
					r.setPosition(y, 1);
					r.get().set(val);
				}
			}
			x++;
		}
		
	}

	
	/**
	 * Return the indice of the begin of the best path
	 * 
	 * @param input the given image
	 * @param paths a matrix of paths
	 * @param mawW the number of paths to consider
	 */
	private static int bestPathHeight(InfPixel[][] paths,int maxH,boolean decrease){
		int best = paths[0][0].val ;
		int minPath = 0;
		if(decrease){
			for(int y=1;y<=maxH;y++){
				if(paths[0][y].val<best){
					best = paths[0][y].val;
					minPath = y;
				}
			}
		}
		else{
			for(int y=1;y<=maxH;y++){
				if(paths[0][y].val>best){
					best = paths[0][y].val;
					minPath = y;
				}
			}
		}
		return minPath;
	}


	/**
	 * Return a matrix of paths, defined by an array of array of InfPixel
	 * 
	 * @param input the given image
	 * @param mawW the width to consider of the image
	 */
	private static InfPixel[][] computePathsHeight(Img<UnsignedByteType> input,int maxH,boolean decrease){
		final RandomAccess<UnsignedByteType> r = input.randomAccess();
        final int iw = (int) input.max(0);
        final int ih = (int) input.max(1);
		InfPixel[][] val = new InfPixel[iw+1][ih+1];
		for(int y=0 ; y<=maxH;y++){
			for(int x=0 ; x<=iw;x++){
				if(y==ih){
					r.setPosition(x, 0);
					r.setPosition(y, 1);
					val[x][y]=new InfPixel(r.get().get(), 0);
				}
				else{
					val[x][y]=new InfPixel(255*ih, 0);
				}
			}
		}
		int g,m,d;
		if(decrease){
			for(int x=iw;x>0;x--){
				for(int y=0;y<=maxH;y++){
					if(y==0){
						r.setPosition(x-1, 0);
						r.setPosition(y, 1);
						m=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y+1, 1);
						d=r.get().get();
						if(m+val[x][y].val<val[x-1][y].val){
							val[x-1][y].val=m+val[x][y].val;
							val[x-1][y].pred=y;
						}
						if(d+val[x][y].val<val[x-1][y+1].val){
							val[x-1][y+1].val=d+val[x][y].val;
							val[x-1][y+1].pred=y;
						}
					}
					else if(y==maxH){
						r.setPosition(x-1, 0);
						r.setPosition(y, 1);
						m=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y-1, 1);
						g=r.get().get();
						if(m+val[x][y].val<val[x-1][y].val){
							val[x-1][y].val=m+val[x][y].val;
							val[x-1][y].pred=y;
						}
						if(g+val[x][y].val<val[x-1][y-1].val){
							val[x-1][y-1].val=g+val[x][y].val;
							val[x-1][y-1].pred=y;
						}
						
					}
					else{
						r.setPosition(x-1, 0);
						r.setPosition(y, 1);
						m=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y-1, 1);
						g=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y+1, 1);
						d=r.get().get();
						
						if(m+val[x][y].val<val[x-1][y].val){
							val[x-1][y].val=m+val[x][y].val;
							val[x-1][y].pred=y;
						}
						if(g+val[x][y].val<val[x-1][y-1].val){
							val[x-1][y-1].val=g+val[x][y].val;
							val[x-1][y-1].pred=y;
						}
						if(d+val[x][y].val<val[x-1][y+1].val){
							val[x-1][y+1].val=d+val[x][y].val;
							val[x-1][y+1].pred=y;
						}
					}
				}
			}
		}
		else{
			for(int x=iw;x>0;x--){
				for(int y=0;y<=maxH;y++){
					if(y==0){
						r.setPosition(x-1, 0);
						r.setPosition(y, 1);
						m=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y+1, 1);
						d=r.get().get();
						if(m+val[x][y].val>val[x-1][y].val){
							val[x-1][y].val=m+val[x][y].val;
							val[x-1][y].pred=y;
						}
						if(d+val[x][y].val>val[x-1][y+1].val){
							val[x-1][y+1].val=d+val[x][y].val;
							val[x-1][y+1].pred=y;
						}
					}
					else if(y==maxH){
						r.setPosition(x-1, 0);
						r.setPosition(y, 1);
						m=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y-1, 1);
						g=r.get().get();
						if(m+val[x][y].val>val[x-1][y].val){
							val[x-1][y].val=m+val[x][y].val;
							val[x-1][y].pred=y;
						}
						if(g+val[x][y].val>val[x-1][y-1].val){
							val[x-1][y-1].val=g+val[x][y].val;
							val[x-1][y-1].pred=y;
						}
						
					}
					else{
						r.setPosition(x-1, 0);
						r.setPosition(y, 1);
						m=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y-1, 1);
						g=r.get().get();
						r.setPosition(x-1, 0);
						r.setPosition(y+1, 1);
						d=r.get().get();
						
						if(m+val[x][y].val>val[x-1][y].val){
							val[x-1][y].val=m+val[x][y].val;
							val[x-1][y].pred=y;
						}
						if(g+val[x][y].val>val[x-1][y-1].val){
							val[x-1][y-1].val=g+val[x][y].val;
							val[x-1][y-1].pred=y;
						}
						if(d+val[x][y].val>val[x-1][y+1].val){
							val[x-1][y+1].val=d+val[x][y].val;
							val[x-1][y+1].pred=y;
						}
					}
				}
			}
		
		}
		return val;
	}

	
	/**
	 * This algorithm is a greedy simplified version of the seam carving
	 * We suppress the sup lower seams of the image
	 * @param input the given image
	 * @param sup number of pixel to reduce
	 */
	public static void greedySeamCarvingHeight(Img<UnsignedByteType> input,Img<UnsignedByteType> output, int sup,boolean decrease){
		final RandomAccess<UnsignedByteType> r = input.randomAccess();
        final int iw = (int) input.max(0);
        final int ih = (int) input.max(1);
		Img<UnsignedByteType> canny;
		
		final RandomAccess<UnsignedByteType> res = output.randomAccess();
		if(!decrease){
			for(int x=0;x<=iw;x++){
				for(int y=0;y<=ih;y++){
					r.setPosition(y, 1);
					res.setPosition(y, 1);
					if(input.numDimensions()==3){

						for(int z=0; z<=2;z++){
							r.setPosition(x, 0);
							res.setPosition(x, 0);
							r.setPosition(z,2);
							res.setPosition(z,2);
							res.get().set(r.get().get());
						}
					}
					else{
						r.setPosition(x,0);
						res.setPosition(x, 0);
						res.get().set(r.get().get());
					}
				}
			}
			canny = output.copy();
			if(output.numDimensions()==3){
				Img<UnsignedByteType> outputCp = output.copy() ;
				Convolution.cannyFilterColor(outputCp, canny, true);
			}
			else{
				Convolution.cannyFilterGrayLevel(output, canny);
			}
		}
		else{
			canny = input.copy();
			if(input.numDimensions()==3){
				Img<UnsignedByteType> inputCp = input.copy() ;
				Convolution.cannyFilterColor(inputCp, canny, true);
			}
			else{
				Convolution.cannyFilterGrayLevel(input, canny);
			}
		}


		for(int x=0;x<sup;x++){
			int maxH;
			if(decrease){maxH = ih-x;}
			else{maxH = ih+x;}
			
			InfPixel[][] paths = new InfPixel[maxH+1][ih+1];
			
			

			paths = computePathsHeight(canny, maxH,decrease);
			
			int bestPath = bestPathHeight(paths, maxH,decrease);
			
			List<Integer> positionsBestPath ;
			positionsBestPath = positionsOfPathHeight(canny, paths, bestPath);
			
			if(!decrease){
				shiftPositionHeightIncrease(output, positionsBestPath, maxH);
				shiftPositionHeight(canny, positionsBestPath,maxH);
			}
			else{
				shiftPositionHeight(input, positionsBestPath,maxH);
				shiftPositionHeight(canny, positionsBestPath,maxH);
			}
			
		}
		if(decrease){
			for(int x=0;x<=iw;x++){
				for(int y=0;y<=ih-sup;y++){
					r.setPosition(y, 1);
					res.setPosition(y, 1);
					if(input.numDimensions()==3){

						for(int z=0; z<=2;z++){
							r.setPosition(x, 0);
							res.setPosition(x, 0);
							r.setPosition(z,2);
							res.setPosition(z,2);
							res.get().set(r.get().get());
						}
					}
					else{
						r.setPosition(x,0);
						res.setPosition(x, 0);
						res.get().set(r.get().get());
					}
				}
			}
		}
		
	}

	
}
