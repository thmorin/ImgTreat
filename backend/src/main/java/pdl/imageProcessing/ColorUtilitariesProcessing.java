package pdl.imageProcessing;

import net.imglib2.RandomAccess;
import net.imglib2.Cursor ;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.numeric.integer.UnsignedByteType;

import net.imglib2.view.Views;
import net.imglib2.view.IntervalView;
import net.imglib2.loops.LoopBuilder;
import pdl.imageProcessing.exception.*;
/**
 * This class factorize some utilitaries about the colors and the image, like converting a colored image on gray, ...
 */
public class ColorUtilitariesProcessing{
	/**
	 * This method is used to change the hue of an already colored image
	 * The change is made on the given image
	 * @param input the colored image that you want to change the hue
	 * @param newHue the new hue after changement
	 */
	public static void colorImg(Img<UnsignedByteType> input, int newHue) throws InvalidParametersException {
		if (newHue < 0 || newHue > 360){ // La demande est bien etrange
			throw new InvalidParametersException("La nouvelle teinte doit être dans [0, 360]") ;
		}
		final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(input, 2, 0), inputG = Views.hyperSlice(input, 2, 1), inputB = Views.hyperSlice(input, 2, 2);
		LoopBuilder.setImages(inputR, inputG, inputB).multiThreaded().forEachPixel(
			// On fait du multithreading : on recoit les pixels, algo simple, pour chaque triplet on fait une conversion en hsv,
			// On change la hue du hsv en newHue, et on convertit le resultat en rgb que l'on set
			(r, g, b) -> {
				int [] rgb = new int[3] ; float [] hsv = new float [3] ;
				rgbToHsv(r.get(), g.get(), b.get(), hsv);
				hsv[0]=newHue ;
				hsvToRgb(hsv[0], hsv[1], hsv[2], rgb);
				r.set(rgb[0]) ; g.set(rgb[1]) ; b.set(rgb[2]) ;
			}
		);
	}

	/**
	 * Transform a colored image on an again colored image, but which represent an image in gray level : all the channel have the same value
	 * The image is transform on the given object by the use of a Loop builder multithreaded
	 * @param img the image that you want to have on gray level
	 */
	public static void colorToGrayLBMultithreaded(Img<UnsignedByteType> img){
		final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0), inputG = Views.hyperSlice(img, 2, 1), inputB = Views.hyperSlice(img, 2, 2);

        LoopBuilder.setImages(inputR, inputG, inputB).multiThreaded().forEachPixel(
            (r, g, b) -> {
				int value = (int) (0.3*r.get() + 0.59*g.get() + 0.11*b.get()) ; // conversion en niveaux de gris
                r.set(value); // On set les trois channels a la meme valeur
				g.set(value); // Ce n'est pas reellement une image en niveaux de gris
				b.set(value); // Mais une image de couleur dont chaque canal a la meme valeur
            }
        );
	}

	/**
	 * Convert a r, g, b color representation to an hsv color representation
	 * @param r the red channel of the rgb color
	 * @param g the green channel of the rgb color
	 * @param b the blue channel of the rgb color
	 * @param hsv the array in which we're going to put the result of the conversion
	 */
	public static void rgbToHsv(int r, int g, int b, float[] hsv){
		// Merci wikipedia pour la conversion
		int max = Math.max(Math.max(r, g), b), min = Math.min(Math.min(r, g), b) ;
		float maxF = max, minF = min, rF = r, gF = g, bF = b ;
		if (max == min)
			hsv[0] = 0 ;
		else if (max == r)
			hsv[0] = (60 * ( (gF-bF)/(maxF - minF) ) + 360)%360 ;
		else if (max == g)
			hsv[0] = 60 * ( (bF-rF)/(maxF - minF) ) + 120 ;
		else
			hsv[0] = 60 * ( (rF-gF)/(maxF - minF) ) + 240 ;
		if (max == 0)
			hsv[1] = 0 ;
		else
			hsv[1] = 1.0F -  minF/ maxF ;
		hsv[2] = ((float) max)/255.0F ;
	}
	/**
	 * Convert a h, s, v color representation to a rgb color representation
	 * @param h the hue channel of the hsv color
	 * @param s the saturation channel of the hsv color
	 * @param v the value channel of the hsv color
	 * @param rgb the array in which we're going to put the result of the conversion
	 */
	public static void hsvToRgb(float h, float s, float v, int[] rgb){
		// Merci wikipedia pour la conversion
		int t = (int) (h/60)%6 ;
		float f = h/60 - t ;
		int l = Math.round(v*(1-s)*255) ;
		int m = Math.round(v * (1- f*s)* 255) ;
		int n = Math.round(v * (1- (1-f)*s)*255) ;
		int nv = Math.round(v*255) ;
		switch(t){
			case(0):
				rgb[0] = nv ; rgb[1] = n; rgb[2] = l ;
				break ;
			case(1):
				rgb[0] = m ; rgb[1] = nv; rgb[2] = l ;
				break ;
			case(2):
				rgb[0] = l ; rgb[1] = nv; rgb[2] = n ;
				break ;
			case(3):
				rgb[0] = l ; rgb[1] = m; rgb[2] = nv ;
				break ;
			case(4):
				rgb[0] = n ; rgb[1] = l; rgb[2] = nv ;
				break ;
			default:
				rgb[0] = nv ; rgb[1] = l; rgb[2] = m ;
				break ;
		}
	}

	/**
	 * Give a gray level image (in two dimensions) from a colored image (in three dimensions)
	 */
	public static Img<UnsignedByteType> Color3DimsImgTo1DimImg(Img<UnsignedByteType> input){
		ArrayImgFactory<UnsignedByteType> factory = new ArrayImgFactory<>(new UnsignedByteType());


		Img<UnsignedByteType> back = factory.create(input.max(0) +1, input.max(1) ) ;
		
		colorToGrayLBMultithreaded(input);

		RandomAccess<UnsignedByteType> rInput = input.randomAccess() ;
		Cursor<UnsignedByteType> cOutput = back.cursor() ;
		rInput.setPosition(0, 2) ;
		while(cOutput.hasNext()){
			cOutput.fwd();
			rInput.setPosition(cOutput.getIntPosition(0), 0) ;
			rInput.setPosition(cOutput.getIntPosition(1), 1) ;
			cOutput.get().set(rInput.get().get()) ;
		}
		return back ;
	}

}
