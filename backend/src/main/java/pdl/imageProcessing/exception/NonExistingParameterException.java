package pdl.imageProcessing.exception;


public class NonExistingParameterException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 2716628836521015188L;

    public NonExistingParameterException(String message) {
        super(message) ;
    }
}