package pdl.imageProcessing.exception;


public class NonExistingAlgorithmException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NonExistingAlgorithmException(String message) {
        super(message) ;
    }
}