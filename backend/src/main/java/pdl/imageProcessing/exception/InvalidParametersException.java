package pdl.imageProcessing.exception;


public class InvalidParametersException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = -4369693419791159110L;

    public InvalidParametersException(String message) {
        super(message) ;
    }
}