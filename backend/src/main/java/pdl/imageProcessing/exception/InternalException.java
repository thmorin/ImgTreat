package pdl.imageProcessing.exception;


public class InternalException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 2073623532164197590L;

    public InternalException(String message) {
        super(message) ;
    }
}