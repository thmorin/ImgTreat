package pdl.imageProcessing;


import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;

/**
 * Thsi class factorize all the methods which make homotheties on images
 */
public class HomothetieProcessing {

	/**
	 * Resize a given image
	 * @param input the given image
	 * @param output the result of the resizing
	 * @param factor the factor of resizing (0.8 corresponds to 0.8 times the size of the image)
	 */
	public static void resizeImg(final Img<UnsignedByteType> input, final Img<UnsignedByteType> output, double factor){
		if (factor < 0){
			throw new RuntimeException(factor+" est trop petit") ;
		}

		// l'idee est la meme que pour resizeImgGrayLevel

		Cursor<UnsignedByteType> cursor = output.cursor() ;
		RandomAccess<UnsignedByteType> rInput = input.randomAccess() ;
		long width = input.dimension(0), height = input.dimension(1) ;

		while (cursor.hasNext()){
			cursor.fwd() ;
			int posX = cursor.getIntPosition(0), posY = cursor.getIntPosition(1)  ;
			// la valeur de ce pixel est celle de (posX/factor, posY/factor, posZ) dans input
			if (output.numDimensions() == 3)
				rInput.setPosition(cursor.getIntPosition(2), 2) ;

			double newX = (posX/factor), newY = (posY/factor) ;
			long longX = (long) newX, longY = (long) newY ;
			if (longX > 0 && longX < width-1 && longY > 0 && longY < height-1){
				rInput.setPosition( longX, 0) ;
				rInput.setPosition( longY, 1) ;

				long fxy = rInput.get().get() ;

				rInput.setPosition( longX+1, 0) ;
				rInput.setPosition( longY, 1) ;
				long fxP1y = rInput.get().get() ;

				rInput.setPosition( longX, 0) ;
				rInput.setPosition( longY+1, 1) ;

				long fxyP1 = rInput.get().get() ;

				rInput.setPosition( longX+1, 0) ;
				rInput.setPosition( longY+1, 1) ;

				long fxP1yP1 = rInput.get().get() ;


				double fxprimey = ((double) fxy) + (newX-Math.floor(longX))*( (double)(fxP1y - fxy)) ;
				double fxprimeyplus1 = ((double) fxyP1) + (newX- Math.floor(longX))*( (double)(fxP1yP1 - fxyP1)) ;
				double fxprimeyprime = ((double) fxprimey) + (newY - Math.floor(longY))* ( (double)(fxprimeyplus1 - fxprimey)) ;

				cursor.get().set( (int)fxprimeyprime) ;

			}
			else {
				rInput.setPosition( longX, 0) ;
				rInput.setPosition( longY, 1) ;
				cursor.get().set(rInput.get().get()) ;
			}

		}
	}
	
	/**
	 * Rotate a given image by an angle tetha in degrees
	 * The rotation is made by the conventional trigonometric sens
	 * @param img the given image
	 * @param output the result of the rotation
	 * @param tetha  the angle in degrees
	 */
    public static void rotate(Img<UnsignedByteType> img, Img<UnsignedByteType> output, int tetha){

		// On applique une rotation
		// On fait une interpolation par plus proche voisin
		// Pour une rotation theta, (on tourne input de tetha degres pour arriver dans output)
		// On effectue en fait l'inverse (On tourne output de -tetha degres pour obtenir input)
		// Cela permet de mieux remplir l'image output (sans cela il y a beaucoup de pixels vides)
		
        final RandomAccess<UnsignedByteType> rInput = img.randomAccess() ;
		final Cursor<UnsignedByteType> cOutput = output.cursor() ;
        double cosT = Math.cos(Math.toRadians(-tetha)), sinT =  Math.sin(Math.toRadians(-tetha));
        int cx = (int) output.max(0)/2, cy = (int) output.max(1)/2 ;
        int newCX = (int) (cosT*cx + sinT*cy), newCY = (int) (-sinT*cx + cosT*cy) ;
        int tx = -newCX+cx, ty = -newCY+cy ;
        long maxW = img.max(0), maxH = img.max(1) ;

		while(cOutput.hasNext()){
			cOutput.fwd() ;
			int x = cOutput.getIntPosition(0), y = cOutput.getIntPosition(1) ;
			int newX = (int) (cosT*x + sinT*y) +tx, newY = (int) (-sinT*x + cosT*y)  + ty;
			if (newX >= 0 && newY >= 0 && newX <= maxW && newY <=maxH){
				rInput.setPosition(newX, 0);
				rInput.setPosition(newY, 1);
				if (output.numDimensions() == 3)
					rInput.setPosition(cOutput.getIntPosition(2), 2);
                cOutput.get().set(rInput.get().get()) ;
				}
			else {
				cOutput.get().set(0) ;
			}
        }
	}

	


}

