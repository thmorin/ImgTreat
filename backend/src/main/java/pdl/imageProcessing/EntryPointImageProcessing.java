package pdl.imageProcessing;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import pdl.imageProcessing.exception.InvalidParametersException;
import org.springframework.data.jpa.repository.JpaRepository;

import pdl.imageProcessing.exception.NonExistingAlgorithmException;
import pdl.imageProcessing.exception.NonExistingParameterException;
import net.imglib2.img.array.ArrayImgFactory;
import javax.imageio.ImageIO;

import net.imglib2.img.Img;
import io.scif.img.ImgOpener;
import io.scif.img.ImgSaver;
import org.scijava.io.location.BytesLocation;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import pdl.backend.Image;
/**
 * This class is used to make an entry point between the image processing and the servor.
 * From some arguments, it returns the byte array corresponding to the image processing
 * This class stores also some utilitaries to facilitate this
 */
public class EntryPointImageProcessing {

    /**
     * This method is used to convert an Image of the servor to an image usable by ImgLib2
     * @param image The image that you want to convert
     * @param onReducedImage If we have to process an image on the reduced version or on the classical
     * @return The converted corresponding imglib2 image
     */
    public static Img<UnsignedByteType> bytesToImg(Image image, boolean onReducedImage) {
        ImgOpener imgOpener = new ImgOpener();
        BytesLocation loc = new BytesLocation( onReducedImage ? image.getReducedImg() : image.getData(), image.getName());
        ArrayImgFactory<UnsignedByteType> factory = new ArrayImgFactory<>(new UnsignedByteType());
        Img<UnsignedByteType> input = (Img<UnsignedByteType>) imgOpener.openImgs(loc, factory).get(0);
        imgOpener.context().dispose();
        return input;
    }

    /**
     * This method is used to convert an image of the imglib2 lib to a byte array corresponding to this image
     * @param image The image stored on the servor which is used to make the imglib2 image
     * @param name the name of the output img
     * @param input the imglib2 image
     * @param onReducedImage If we have to process an image on the reduced version or on the classical
     * @return a byte array which correspond to all the parameters
     */
    public static byte[] ImgToBytes(Image image, String name, Img<UnsignedByteType> input, boolean onReducedImage) {
        BytesLocation locBack = new BytesLocation(onReducedImage ? image.getReducedImg().length : image.getData().length, name);
        ImgSaver saver = new ImgSaver();
        saver.saveImg(locBack, input);
        saver.context().dispose();
        byte[] backData = locBack.getByteBank().toByteArray();
        return backData;
    }

    /**
     * create an imglib2 coloredImage
     */
    public static Img<UnsignedByteType> createColoredImg(int width, int height, String subtype, String name){
        BufferedImage bufferedImage = new BufferedImage( width,
        height, BufferedImage.TYPE_INT_RGB);
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream() ;
        try {
            if (subtype.equals("jpeg"))
                ImageIO.write(bufferedImage, "JPEG", bos);
            else {
                ImageIO.write(bufferedImage, "TIFF", bos);
            }
        } catch (IOException e){
            throw new RuntimeException(e) ;
        }
        byte [] bytes = bos.toByteArray() ;

        ImgOpener imgOpener = new ImgOpener();
        BytesLocation loc = new BytesLocation(bytes, name);
        ArrayImgFactory<UnsignedByteType> factory = new ArrayImgFactory<>(new UnsignedByteType());
        Img<UnsignedByteType> output = (Img<UnsignedByteType>) imgOpener.openImgs(loc, factory).get(0);
        imgOpener.context().dispose();
        return output ;
    }

    /**
     * This method is used to make from an image stored on the servor and some arguments an image processing corresponding to
     * the arguments.
     * @param image The image stored on the servor
     * @param params the params which correspond to the wanted processing
     * @param imageDao the object which store all the images : usefull for the binary image operations
     * @param onReducedImage if we make the algorithm on the full size image or on the reduced image
     * @return a byte array corresponding to an image with the good image processing
     * @throws InvalidParametersException if the algorithm with the parameters' given array doesn't exist
     * @throws NonExistingAlgorithmException if the algorithm doesn't exist
     * @throws NonExistingParameterException the parameter doesn't exist
     */
    public static byte[] algorithmEntryPoint(Image image, String[] params, JpaRepository<Image,Long> imageDao, boolean onReducedImage)
            throws InvalidParametersException, NonExistingAlgorithmException, NonExistingParameterException {
        Img<UnsignedByteType> input = bytesToImg(image, onReducedImage); // recuperation de l'image en tableau de bytes en image imglib2
        if (params[0] == null)
            throw new InvalidParametersException("need an algorithm") ;
        if (params[0].equals("increaseLuminosity")) {
            if (params[1] == null)
                throw new NonExistingParameterException("need a gain to be precised") ;
            String[] paramGain = params[1].split("=");
            if (paramGain.length != 2) {
                throw new InvalidParametersException("Need a gain to be precised");
            }
            if (paramGain[0].equals("gain")) {
                int gain = 0;
                try {
                    gain = Integer.parseInt(paramGain[1]);
                } catch (NumberFormatException e) {
                    throw new InvalidParametersException("The gain has to be a number");
                }
                LuminosityProcessing.changeLuminosityCursorClassic(input, gain);
                return ImgToBytes(image, "modif." + image.getType().getSubtype(), input, onReducedImage);
            } else {
                throw new InvalidParametersException(
                        "Le premier paramètre doit être une précision du gain de luminosité");
            }
        } else if (params[0].equals("histogramEqualization")) {
            if (image.getNbSlices() == 1) { // image noir et blanc
                ContrastProcessing.histogramEqualizationGrayLevel(input);
            } else {
                // image couleur : precision du canal
                if (params[1] == null)
                    throw new InvalidParametersException(params[1] + " is not valid for an histogram equalization algorithm");
                switch (params[1]){
                    case "channel=V" :
                        ContrastProcessing.histogramEqualizationColor_ValueChannel(input);
                        break ;
                    case "channel=S" :
                        ContrastProcessing.histogramEqualizationColor_SaturationChannel(input);
                        break ;
                    default :
                        throw new InvalidParametersException(params[1] + " is not valid for an histogram equalization algorithm");
                }
            }
            return ImgToBytes(image, "modif." + image.getType().getSubtype(), input, onReducedImage);
        } else if (params[0].equals("coloredFilter")) {
            if (image.getNbSlices() != 3) {
                // image en NB
                throw new NonExistingParameterException(
                        "Le changement de teinte ne peut pas être appliqué à une image noir et blanc");
            }
            if (params[1] == null)
                throw new NonExistingParameterException("need a hue to be precised") ;
            String[] paramHue = params[1].split("=");
            if (paramHue.length != 2) {
                throw new InvalidParametersException("Une nouvelle teinte doit être précisée");
            }
            if (paramHue[0].equals("hue")) {
                int hue = 0;
                try {
                    hue = Integer.parseInt(paramHue[1]);
                } catch (NumberFormatException e) {
                    throw new InvalidParametersException("La teinte doit être un nombre");
                }
                if (hue > 360 || hue < 0)
                    throw new InvalidParametersException("La teinte doit être comprise entre 0 et 360");
                ColorUtilitariesProcessing.colorImg(input, hue);
                return ImgToBytes(image, "modif." + image.getType().getSubtype(), input, onReducedImage);
            } else
                throw new InvalidParametersException("Le premier paramètre doit être une précision de la nouvelle teinte");
        } else if (params[0].equals("blurryFilter")) {
            // on commence par lire les parametres, on lancera ensuite les methodes
            Img<UnsignedByteType> output = input.copy() ;
            int size = 0 ;
            final int MEAN = 0, GAUSSIAN = 1, MEDIAN = 2, BILATERAL = 3 ;
            int typeAlgo = -1 ;
            if (params[1] == null)
                throw new NonExistingParameterException("need a type to be precised") ;
            switch (params[1]){ // lecture du type
                case ("type=mean") :
                    typeAlgo = MEAN ;
                    break ;
                case ("type=gaussian") :
                    typeAlgo = GAUSSIAN ;
                    break ;
                case ("type=median") :
                    typeAlgo = MEDIAN ;
                    break ;
                case ("type=bilateral") :
                    typeAlgo = BILATERAL ;
                    break ;
                default :
                    throw new NonExistingParameterException("need a type to be precised") ;
            }
            // lecture de la taille
            if (params[2] == null)
                throw new NonExistingParameterException("une taille doit être précisée") ;
            String[] paramSize = params[2].split("=");
            if (paramSize.length != 2)
                throw new InvalidParametersException("Une taille de convolution doit être précisée");
            if (paramSize[0].equals("size")) {
                try {
                    size = Integer.parseInt(paramSize[1]);
                } catch (NumberFormatException e) {
                    throw new InvalidParametersException("Le taille de convolution doit être un nombre");
                }
            }
            else throw new NonExistingParameterException("une taille doit être précisée") ;
            switch(typeAlgo){
                case MEAN :
                    Convolution.meanFilter(input, output, size);
                    break ;
                case GAUSSIAN :
                    Convolution.gaussFilter(size, input, output);
                    break ;
                case MEDIAN :
                    Convolution.medianFilter(input, output, size);
                    break ;
                case BILATERAL :
                    Convolution.bilateralFilter(input, output, size);
                    break ;
            }
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output, onReducedImage) ;
        }
        else if (params[0].equals("sobelFilter")) {
            boolean isGrayLevel = (image.getNbSlices() == 1), isColored = false;
            Img<UnsignedByteType> output = input.copy() ;
            if (params[1] == null) 
                throw new NonExistingParameterException("need a color to be precised") ;
            if (!isGrayLevel){
                switch(params[1]){
                    case ("colored=yes") :
                        isColored = true ;
                        break ;
                    case("colored=no") :
                        isColored = false ;
                        break ;
                    default :
                        throw new NonExistingParameterException("need a color to be precised") ;
                }
            }
            Convolution.sobelFilter(input, output, isColored);
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output, onReducedImage) ;
        }
        else if (params[0].equals("cannyFilter")) {
            boolean isGrayLevel = (image.getNbSlices() == 1), isColored = false;
            Img<UnsignedByteType> output = input.copy() ;
            if (params[1] == null) 
                throw new NonExistingParameterException("need a color to be precised") ;
            if (!isGrayLevel){
                switch(params[1]){
                    case ("colored=yes") :
                        isColored = true ;
                        break ;
                    case("colored=no") :
                        isColored = false ;
                        break ;
                    default :
                        throw new NonExistingParameterException("need a color to be precised") ;
                }
            }
            if (isGrayLevel)
                Convolution.cannyFilterGrayLevel(input, output);
            else
                Convolution.cannyFilterColor(input, output, isColored) ;
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output, onReducedImage) ;
        }
        else if (params[0].equals("cartoonEffect")){
            ArtProcessing.cartoonEffect(input);
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , input, onReducedImage) ;
        }
        else if (params[0].equals("erosionEffect")){
            Img<UnsignedByteType> output = input.copy() ;
            MorphologicOperations.erosion(input, output);
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output, onReducedImage) ;
        }
        else if (params[0].equals("dilatationEffect")){
            Img<UnsignedByteType> output = input.copy() ;
            MorphologicOperations.dilatation(input, output);
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output, onReducedImage) ;
        }
        else if (params[0].equals("sketchEffect")){
            ArtProcessing.sketchEffect(input);
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , input, onReducedImage) ;
        }
        else if (params[0].equals("oilPaintingEffect")){
            Img<UnsignedByteType> output = input.copy() ;
            ArtProcessing.oilPaintingEffect(input, output);
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output, onReducedImage) ;
        }
        else if (params[0].equals("resize")){
            boolean isGrayLevel = (image.getNbSlices() == 1) ;
            Img<UnsignedByteType> output = null ;
            double factor = 0. ;
            if (params[1] == null )
                throw new InvalidParametersException("il faut préciser un facteur de changement de taille") ;
            String [] paramNewSize = params[1].split("=") ;
            if (paramNewSize.length != 2 || !paramNewSize[0].equals("factor"))
                throw new InvalidParametersException("il faut préciser un facteur de changement de taille") ;
            try {
                factor = Double.parseDouble(paramNewSize[1]) ;
            }
            catch (NumberFormatException e){
                throw new InvalidParametersException(paramNewSize[1] + " n'est pas un facteur valide") ;
            }
            if ((long) (image.getWidth()*factor) <= 0 || (long) (image.getHeight()*factor) <= 0 )
                throw new InvalidParametersException("L'image est trop petite pour être redimensionnée à un facteur "+ factor) ;
            if (isGrayLevel) {
                output = new ArrayImgFactory< UnsignedByteType >(new UnsignedByteType())
                .create( new long[] { (long) ( (onReducedImage ? image.getLittleWidth() : image.getWidth())*factor), (long) ((onReducedImage ? image.getLittleHeight() : image.getHeight())*factor) } );
            }
            else {
                output = createColoredImg((int) ( (onReducedImage ? image.getLittleWidth() : image.getWidth())*factor), (int) ((onReducedImage ? image.getLittleHeight() : image.getHeight())*factor), image.getType().getSubtype(),
                image.getName()) ;
            }
            HomothetieProcessing.resizeImg(input, output, factor);
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output, onReducedImage) ;
        }
        else if (params[0].equals("rotate")){
            int angle = 0 ;
            final Img< UnsignedByteType > output = input.copy() ;
            if (params[1] == null)
                throw new InvalidParametersException("il faut préciser un angle de rotation") ;
            String[] paramNewSize = params[1].split("=");
            if (paramNewSize.length != 2 || !paramNewSize[0].equals("angle")){
                throw new InvalidParametersException("il faut préciser un angle de rotation") ;
            }
            try {
                angle = Integer.parseInt(paramNewSize[1]) ;
            }
            catch (NumberFormatException e){
                throw new InvalidParametersException(paramNewSize[1] + " n'est pas un angle valide") ;
            }
            HomothetieProcessing.rotate(input, output, angle);
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output, onReducedImage) ;
        }
        else if (params[0].equals("seamK")){
            int newWidth = 0, newHeight = 0 ;
            Img< UnsignedByteType > output ;
            if (params[1] == null || params[2] == null)
                throw new InvalidParametersException("il faut préciser une nouvelle taille") ;
            String[] paramNewWidth = params[1].split("="), paramNewHeight = params[2].split("=");
            if (paramNewWidth.length != 2 || !paramNewWidth[0].equals("width") || paramNewHeight.length != 2 || !paramNewHeight[0].equals("height")){
                throw new InvalidParametersException("il faut préciser une nouvelle taille") ;
            }
            try {
                newWidth = Integer.parseInt(paramNewWidth[1]) ;
                newHeight = Integer.parseInt(paramNewHeight[1]) ;
            }
            catch (NumberFormatException e){
                throw new InvalidParametersException("(" + paramNewWidth[1]+", "+ paramNewHeight[1] +") n'est pas une largeur valide valide") ;
            }
            if (image.getNbSlices() == 1) {
                output = new ArrayImgFactory< UnsignedByteType >(new UnsignedByteType())
                .create( new long[] { (long) (newWidth), (long) (newHeight) } );
            }
            else {
                output = createColoredImg(newWidth, newHeight, image.getType().getSubtype(),
                image.getName()) ;
            }
            SeamCarving.simplifiedSeamCarving(input, output, newWidth, newHeight);
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output, onReducedImage) ;
        }

        else if (params[0].equals("seamCW")){
            int newWidth = 0, newHeight = 0 ;
            Img< UnsignedByteType > output ;
            Img< UnsignedByteType > output2 ;
            if (params[1] == null || params[2] == null)
                throw new InvalidParametersException("il faut préciser une nouvelle taille") ;
            String[] paramNewWidth = params[1].split("="), paramNewHeight = params[2].split("=");
            if (paramNewWidth.length != 2 || !paramNewWidth[0].equals("width") || paramNewHeight.length != 2 || !paramNewHeight[0].equals("height")){
                throw new InvalidParametersException("il faut préciser une nouvelle taille") ;
            }
            try {
                newWidth = Integer.parseInt(paramNewWidth[1]) ;
                newHeight = Integer.parseInt(paramNewHeight[1]) ;
            }
            catch (NumberFormatException e){
                throw new InvalidParametersException("(" + paramNewWidth[1]+", "+ paramNewHeight[1] +") n'est pas une largeur valide valide") ;
            }
            if (image.getNbSlices() == 1) {
                output = new ArrayImgFactory< UnsignedByteType >(new UnsignedByteType())
                .create( new long[] { image.getWidth(), (long) (newHeight) } );
                output2= new ArrayImgFactory< UnsignedByteType >(new UnsignedByteType())
                .create( new long[] { (long) (newWidth), (long)(newHeight) } );
            }
            else {
                output = createColoredImg((int)image.getWidth(), newHeight, image.getType().getSubtype(),
                image.getName()) ;
                output2 = createColoredImg(newWidth, newHeight, image.getType().getSubtype(),
                image.getName()) ;
            }
            if(newHeight>image.getHeight()){
                SeamCarving.greedySeamCarvingHeight(input, output,(int)(newHeight -image.getHeight()),false);
            }
            else{
                SeamCarving.greedySeamCarvingHeight(input, output,(int)(image.getHeight()-newHeight),true);
            }
            if(newWidth>image.getWidth()){
                SeamCarving.greedySeamCarvingWidth(output, output2,(int)(newWidth -image.getWidth()),false);
            }
            else{
                SeamCarving.greedySeamCarvingWidth(output, output2,(int)(image.getWidth()-newWidth ),true);
            }
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output2, onReducedImage) ;
        }

        else if (params[0].equals("add") || params[0].equals("substract") || params[0].equals("divide") ){
            int idSecondImg = 0 ;
            Img<UnsignedByteType> img2, output = null ;
            if (params[1] == null)
                throw new InvalidParametersException("il faut préciser uen image d'addition") ;
            String[] paramSecondImg = params[1].split("=");
            if (paramSecondImg.length != 2 || !paramSecondImg[0].equals("id")){
                throw new InvalidParametersException("il faut préciser une image d'addition") ;
            }
            try {
                idSecondImg = Integer.parseInt(paramSecondImg[1]) ;
            }
            catch (NumberFormatException e){
                throw new InvalidParametersException(paramSecondImg[1] + " n'est pas une image valide") ;
            }
            if (!imageDao.existsById((long) idSecondImg))
                throw new NonExistingParameterException(idSecondImg +" n'est pas un id existant") ;
            Image image2 = imageDao.getOne( (long) idSecondImg) ;
            img2 = bytesToImg(image2, onReducedImage) ;
            long width = Math.max(input.max(0), img2.max(0) ) + 1, height = Math.max(input.max(1), img2.max(1)) + 1 ;
            if (image.getNbSlices() != image2.getNbSlices())
                throw new InvalidParametersException("Les images d'additions doivent avoir le meme nombre de slices") ;
            if ( ! image.getType().getSubtype().equals(image2.getType().getSubtype()))
                throw new InvalidParametersException("Les images d'additions doivent avoir la meme extension") ;
            if (image.getNbSlices() == 1){
               output = new ArrayImgFactory< UnsignedByteType >(new UnsignedByteType())
                .create( new long[] { width, height } );
            }
            else {
                output = createColoredImg((int)width, (int) height, image.getType().getSubtype(), image.getName()) ;
            }
            switch(params[0]){
                case "add" :
                    OperationImgProcessing.addImg(input, img2, output);
                    break ;
                case "substract" :
                    OperationImgProcessing.subImg(input, img2, output);
                    break ;
                case "divide" : // todo en couleur
                    OperationImgProcessing.divideImg(input, img2, output);
                    break ;
                default :
                    throw new NonExistingAlgorithmException("Cet algorithme n'existe pas");
            }
            return ImgToBytes(image, "modif."+image.getType().getSubtype() , output, onReducedImage) ;
        }
        throw new NonExistingAlgorithmException("Cet algorithme n'existe pas : donner un paramètre valide");
    }
}