package pdl.imageProcessing;

import net.imglib2.RandomAccess;
import net.imglib2.Cursor ;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.IntType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import java.util.Arrays;
import java.util.List;

import net.imglib2.view.Views;
import net.imglib2.view.IntervalView;
import net.imglib2.loops.LoopBuilder;

/**
 * This class factorize the methods of processing the contrast issues
 * it concerns comuting histogram, dynamic extension, histogram equalization ...
 */
public class ContrastProcessing{

    /**
     * Method used to make an histogram of a gray level image
     * @param img the image that you would like the histogram
     * @return the corresponding histogram
     */
	public static int [] histogramGrayLevel(Img<UnsignedByteType> img){
		final Cursor<UnsignedByteType> c = img.cursor() ;

		int histogram[] = new int[256] ;
		Arrays.fill(histogram, 0) ; // assurance d'avoir un tableau rempli de 0 au depart
		while(c.hasNext()){
			c.fwd();
			final UnsignedByteType val = c.get();
			histogram[val.get()]++ ;
		}
		return histogram ;
	}

    /**
     * This method is used to make an histogram equalization on colored image with regards to the conversion on grayLevel
     * @param img the image that you would like to equalize. The equalization is made on the image --> It is modified
     */
	public static void histogramEqualizationColor_GrayChannel(Img<UnsignedByteType> img){
		final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0), inputG = Views.hyperSlice(img, 2, 1), inputB = Views.hyperSlice(img, 2, 2);
		final IntType valueInt1 = new IntType(1) ;

		// On doit creer un histogramme en niveaux de gris de l'image.
		// Probleme : complique au premier abord a multithread : histogram est une variable partagee donc un lock fait perdre en temps
		// Meilleure idee : chaque thread travaille sur une partie distincte de l'image ( un chunk) avec son propre
		// tableau d'histogramme partiel
		// Quand tous les threads ont bosse, on reduit les histogrammes partiels (on les ajoute variable par variable)
		// Afin d'obtenir un histogramme complet
		List<IntType[]> listOfHistograms = LoopBuilder.setImages(inputR, inputG, inputB).multiThreaded().forEachChunk(
			chunk -> {
				IntType [] histogram = new IntType[256] ; // histogramme partiel
				for(int i = 0; i < 256; i++)
					histogram[i] = new IntType(0) ; // initialisation
				chunk.forEachPixel((r,g,b) -> {
					histogram[(int) Math.round(0.3*r.get() + 0.59*g.get() + 0.11*b.get())].add(valueInt1); // Conversion en niveaux de gris
				});
				return histogram ;
			}
		) ;
		IntType[] completeHistogram = new IntType[256] ; // histoframme complet
		for(int i = 0; i < 256; i++)
			completeHistogram[i] = new IntType(0) ; 

		listOfHistograms.forEach(histogram -> { // boucle de reduction
			for(int i = 0; i < 256; i++){
				completeHistogram[i].add(histogram[i]) ;
			}
		});

		int cumulatedHistogram[] = new int[256] ; // ici c'est l'histogramme cumule classique
		cumulatedHistogram[0] = completeHistogram[0].get() ;
		for(int i = 1; i< 256; i++){
			cumulatedHistogram[i] = cumulatedHistogram[i-1] + completeHistogram[i].get() ;
		}
		int lookUpTable[] = new int[256] ;
		for(int i = 0; i < 256; i++){
			// look up table
			lookUpTable[i] = cumulatedHistogram[i]*255/ ((int)img.size()) ; // on fait une lut pour accelerer le calcul
		}
		// ici pas de probleme de multithreading : lookUpTable est partagee mais les threads ne font que la lire
		// Donc pas de concurrence
		LoopBuilder.setImages(inputR, inputG, inputB).multiThreaded().forEachPixel(
			(r, g, b) -> {
				r.set(lookUpTable[r.get()]) ; g.set(lookUpTable[g.get()]) ; b.set(lookUpTable[b.get()]) ;
			}
		);
	}
    /**
     * This method is used to make an histogram equalization on colored image with regards to the value channel
     * @param img the image that you would like to equalize. The equalization is made on the image --> It is modified
     */
	public static void histogramEqualizationColor_ValueChannel(Img<UnsignedByteType> img){
		final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0), inputG = Views.hyperSlice(img, 2, 1), inputB = Views.hyperSlice(img, 2, 2);
		final IntType valueInt1 = new IntType(1) ;
		// we consider the v of hsv as an int between 0 and 100

		// L'idee est identique a histogramEqualizationColor_GrayChannel mais sur le canal value
		List<IntType[]> listOfHistograms = LoopBuilder.setImages(inputR, inputG, inputB).multiThreaded().forEachChunk(
			chunk -> {
				IntType [] histogram = new IntType[101] ;
				float [] hsv = new float[3] ;
				for(int i = 0; i <= 100; i++)
					histogram[i] = new IntType(0) ;
				chunk.forEachPixel((r,g,b) -> {
					ColorUtilitariesProcessing.rgbToHsv(r.get(), g.get(), b.get(), hsv);
					histogram[(int) Math.round(hsv[2]*100)].add(valueInt1) ;
				});
				return histogram ;
			}
		) ;
		IntType[] completeHistogram = new IntType[101] ;
		for(int i = 0; i <= 100; i++)
			completeHistogram[i] = new IntType(0) ;

		listOfHistograms.forEach(histogram -> {
			for(int i = 0; i <= 100; i++){
				completeHistogram[i].add(histogram[i]) ;
			}
		});

		int cumulatedHistogram[] = new int[101] ;
		cumulatedHistogram[0] = completeHistogram[0].get() ;
		for(int i = 1; i<= 100; i++){
			cumulatedHistogram[i] = cumulatedHistogram[i-1] + completeHistogram[i].get() ;
		}
		int lookUpTable[] = new int[101] ;
		for(int i = 0; i <= 100; i++){
			// look up table
			lookUpTable[i] = cumulatedHistogram[i]*100/ ((int)(img.size()/3)) ;
		}
		LoopBuilder.setImages(inputR, inputG, inputB).multiThreaded().forEachPixel(
			(r, g, b) -> {
				float [] hsv = new float[3] ;
				int [] rgb = new int [] {r.get(), g.get(), b.get()} ;
				ColorUtilitariesProcessing.rgbToHsv(rgb[0], rgb[1], rgb[2], hsv);
				hsv[2] = ((float) lookUpTable[(int)Math.round(hsv[2]*100)])/100 ;
				ColorUtilitariesProcessing.hsvToRgb(hsv[0], hsv[1], hsv[2], rgb);
				r.set(rgb[0]) ; g.set(rgb[1]) ; b.set(rgb[2]) ;
			}
		);
	}
    /**
     * This method is used to make an histogram equalization on colored image with regards to the stauration channel
     * @param img the image that you would like to equalize. The equalization is made on the image --> It is modified
     */
	public static void histogramEqualizationColor_SaturationChannel(Img<UnsignedByteType> img){
		final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0), inputG = Views.hyperSlice(img, 2, 1), inputB = Views.hyperSlice(img, 2, 2);
		final IntType valueInt1 = new IntType(1) ;
		// we consider the s of hsv as an int between 0 and 100

		// L'idee est identique a histogramEqualizationColor_GrayChannel mais sur le canal saturation
		List<IntType[]> listOfHistograms = LoopBuilder.setImages(inputR, inputG, inputB).multiThreaded().forEachChunk(
			chunk -> {
				IntType [] histogram = new IntType[101] ;
				float [] hsv = new float[3] ;
				for(int i = 0; i <= 100; i++)
					histogram[i] = new IntType(0) ;
				chunk.forEachPixel((r,g,b) -> {
					ColorUtilitariesProcessing.rgbToHsv(r.get(), g.get(), b.get(), hsv);
					histogram[(int) Math.round(hsv[1]*100)].add(valueInt1) ;
				});
				return histogram ;
			}
		) ;
		IntType[] completeHistogram = new IntType[101] ;
		for(int i = 0; i <= 100; i++)
			completeHistogram[i] = new IntType(0) ;

		listOfHistograms.forEach(histogram -> {
			for(int i = 0; i <= 100; i++){
				completeHistogram[i].add(histogram[i]) ;
			}
		});

		int cumulatedHistogram[] = new int[101] ;
		cumulatedHistogram[0] = completeHistogram[0].get() ;
		for(int i = 1; i<= 100; i++){
			cumulatedHistogram[i] = cumulatedHistogram[i-1] + completeHistogram[i].get() ;
		}
		int lookUpTable[] = new int[101] ;
		for(int i = 0; i <= 100; i++){
			// look up table
			lookUpTable[i] = cumulatedHistogram[i]*100/ ((int)(img.size()/3)) ;
		}
		LoopBuilder.setImages(inputR, inputG, inputB).multiThreaded().forEachPixel(
			(r, g, b) -> {
				float [] hsv = new float[3] ;
				int [] rgb = new int [] {r.get(), g.get(), b.get()} ;
				ColorUtilitariesProcessing.rgbToHsv(rgb[0], rgb[1], rgb[2], hsv);
				hsv[1] = ((float) lookUpTable[(int)Math.round(hsv[1]*100)])/100 ;
				ColorUtilitariesProcessing.hsvToRgb(hsv[0], hsv[1], hsv[2], rgb);
				r.set(rgb[0]) ; g.set(rgb[1]) ; b.set(rgb[2]) ;
			}
		);
	}

    /**
     * This method is used to make an histogram equalization on a gray level image
     * @param img the image that you would like to equalize. The equalization is made on the image --> It is modified
     */
    public static void histogramEqualizationGrayLevel(Img<UnsignedByteType> img){

		final Cursor<UnsignedByteType> c = img.cursor() ;

		// first we compute the histogram
		int histogram[] = new int[256] ;

		Arrays.fill(histogram, 0) ;
		while(c.hasNext()){
			c.fwd();
			final UnsignedByteType val = c.get();
			histogram[val.get()]++ ;
		}
		// second we compute the cumulated histogram
		int cumulatedHistogram[] = new int[256] ;
		cumulatedHistogram[0] = histogram[0] ;
		for(int i = 1; i< 256; i++){
			cumulatedHistogram[i] = cumulatedHistogram[i-1] + histogram[i] ;
		}
		int lookUpTable[] = new int[256] ;
		for(int i = 0; i < 256; i++){
			// look up table
			lookUpTable[i] = cumulatedHistogram[i]*255/ ((int)img.size()) ;
		}
		c.reset(); ;
		// now we make the dynamic extension
		while(c.hasNext()){
			c.fwd();
			final int val = c.get().get();
			c.get().set(lookUpTable[val]) ;
		}
	}

    /**
     * This method is used to improve the contrast of a gray level image by making a dynamic extension with a new minimum and a new maximum
     * @param img the image that you would llike to extend
     * @param newMin the new minimum of luminosity
     * @param newMax the new maximum of luminosity
     */
	public static void dynamicExtensionGrayLevel(Img<UnsignedByteType> img, int newMin, int newMax){
		int min = 256, max = -1 ;
		Cursor<UnsignedByteType> c = img.cursor() ;
		//first we compute the min and the max values of the histogram
		while(c.hasNext()){
			c.fwd();
			final UnsignedByteType val = c.get();
			if (val.get() < min) min = val.get() ;
			if (val.get() > max) max = val.get() ;
		}

		int lookUpTable[] = new int[max-min+1] ;
		for(int i = min; i <= max; i++){
			// look up table formation
			lookUpTable[i-min] = (newMax-newMin)*(i-min)/(max - min +1) + newMin;
		}

		c = img.cursor() ;
		// now we make the dynamic extension
		while(c.hasNext()){
			c.fwd();
			final UnsignedByteType val = c.get();
			val.set(lookUpTable[val.get()-min]) ;
		}
	}



    /**
     * This method is used to make an dynamic ewtension of luminosity on colored image with regards to the conversion on grayLevel
     * @param img the image that you would like to equalize. The equalization is made on the image --> It is modified
     * @param newMin the new minimum value of a pixel
     * @param newMax the new maximum value of a pixel
     */
	public static void dynamicExtensionColor_GrayChannel(Img<UnsignedByteType> img, int newMin, int newMax){
		final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0), inputG = Views.hyperSlice(img, 2, 1), inputB = Views.hyperSlice(img, 2, 2);
		List<IntType[]> listOfMins = LoopBuilder.setImages(inputR, inputG, inputB).multiThreaded().forEachChunk(
			chunk -> {
				IntType[] values = new IntType[]{new IntType(256), new IntType(-1)} ;
				chunk.forEachPixel((r,g,b) -> {
					int grayValue = (int) (0.3*r.get() + 0.59*g.get() + 0.11*b.get()) ;
					values[0].set(Math.min(values[0].get(), grayValue )) ;
					values[1].set(Math.max(values[1].get(), grayValue )) ;
				});
				return values ;
			}
		) ;

		IntType[] globalValues = new IntType[]{new IntType(256), new IntType(-1)} ;
		listOfMins.forEach( value -> {
			globalValues[0].set(Math.min(value[0].get(), globalValues[0].get())) ;
			globalValues[1].set(Math.max(value[1].get(), globalValues[1].get())) ;
		}) ;

		int intMin = globalValues[0].get(), intMax = globalValues[1].get()  ;

		int lookUpTable[] = new int[256] ;
		for(int i = 0; i < 256; i++){
			// look up table formation
			lookUpTable[i] = (newMax-newMin)*(i-intMin)/(intMax - intMin) + newMin;
		}

		LoopBuilder.setImages(inputR, inputG, inputB).forEachPixel(
            (r, g, b) -> {
                r.set(lookUpTable[r.get()]);
				g.set(lookUpTable[g.get()]);
				b.set(lookUpTable[b.get()]);
            }
        );

	}
    /**
     * This method is used to make an dynamic extension of luminosity on colored image with regards to the valeu channel
     * @param img the image that you would like to equalize. The equalization is made on the image --> It is modified
     * @param newMin the new minimum value of a pixel
     * @param newMax the new maximum value of a pixel
     */
	public static void dynamicExtensionColor_ValueChannel(Img<UnsignedByteType> img, int newMin, int newMax){
		final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0), inputG = Views.hyperSlice(img, 2, 1), inputB = Views.hyperSlice(img, 2, 2);
		if (newMin < 0 || newMax < 0 || newMin >100 || newMax > 100){
			System.err.println("les nouvelles valeurs de min et max doivent être dans [0, 100]") ;
		}

		List<IntType[]> listOfExtrema =LoopBuilder.setImages(inputR, inputG, inputB).multiThreaded().forEachChunk(
			chunk -> {
				IntType[] extrema = new IntType[] {new IntType(101), new IntType(-1)} ; // we consider v has an int value between 0 and 100
				chunk.forEachPixel((r,g,b) -> {
					float [] hsv = new float[3] ;
					ColorUtilitariesProcessing.rgbToHsv(r.get(),g.get(),b.get(), hsv) ;
					int value = (int) (hsv[2]*100) ;
					extrema[0].set(Math.min(extrema[0].get(), value)) ; // extrema[0] is the min
					extrema[1].set(Math.max(extrema[1].get(), value)) ; // extrema[1] is the max
				}) ;
				return extrema ;
			}
		) ; // we have in listOfExtrema the locals extrema

		IntType[] globalExtrema = new IntType[] {new IntType(101), new IntType(-1)} ; // we consider v has an int value between 0 and 100

		listOfExtrema.forEach( extrema -> {
			globalExtrema[0].set(Math.min(globalExtrema[0].get(), extrema[0].get())) ;
			globalExtrema[1].set(Math.max(globalExtrema[1].get(), extrema[1].get())) ;
		} );

		int intMin = globalExtrema[0].get(), intMax = globalExtrema[1].get() ;

		int [] lookUpTable = new int[intMax - intMin + 1] ;

		for(int i = intMin ; i <= intMax; i++){
			//making the lut
			lookUpTable[i-intMin] = (newMax-newMin)*(i-intMin)/(intMax - intMin + 1) + newMin ;
		}

		LoopBuilder.setImages(inputR, inputG, inputB).forEachPixel(
            (r, g, b) -> {
				float [] hsv = new float[3] ;
				ColorUtilitariesProcessing.rgbToHsv(r.get(), g.get(), b.get(), hsv);
				int value = (int) (hsv[2]*100) ;
				value = lookUpTable[value-intMin] ;

				hsv[2] = ((float) value)/100 ;
				int [] rgb = new int[3] ;
				ColorUtilitariesProcessing.hsvToRgb(hsv[0], hsv[1], hsv[2], rgb);
				r.set(rgb[0]) ; g.set(rgb[1]) ; b.set(rgb[2]) ;
            }
		);

	}

    /**
     * Threshold of a gray level image
     * @param img the given image
     * @param t the threshold
     */
    public static void threshold(Img<UnsignedByteType> img, int t) {
		final RandomAccess<UnsignedByteType> r = img.randomAccess();

		final int iw = (int) img.max(0);
		final int ih = (int) img.max(1);

		for (int x = 0; x <= iw; ++x) {
			for (int y = 0; y <= ih; ++y) {
				r.setPosition(x, 0);
				r.setPosition(y, 1);
				final UnsignedByteType val = r.get();
				if (val.get() < t)
				    val.set(0);
				else
				    val.set(255);
			}
		}
	}

}
