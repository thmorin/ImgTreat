package pdl.backend;
import java.io.File;
import java.util.ArrayList;
import org.springframework.http.MediaType;
import org.springframework.util.MimeType ;
import java.awt.image.BufferedImage;
import java.nio.file.Files;
import javax.imageio.ImageIO;
import java.io.IOException;
import org.springframework.core.io.ClassPathResource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BackendApplication {


	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}
	public static void getAllFiles(File file, ArrayList<File> list) {
		if (file.isFile()) {
			list.add(file) ;
			return ;
		}
		for(File f: file.listFiles()) {
			getAllFiles(f, list);
		}
	}
	
  	@Bean
 	public CommandLineRunner demo(ImageDao repository) {
    return (args) -> {

		final ClassPathResource imgFold = new ClassPathResource("images");
		if (!imgFold.exists() ){
		  // le dossier n'existe pas
		  throw new RuntimeException("Le dossier images n'existe pas : " ) ;
		}
		try {
			File imgDir = imgFold.getFile() ;
			ArrayList<File> files =  new ArrayList<File>() ;
			getAllFiles(imgDir, files);
			for(File f: files) {
			  if ( Files.probeContentType(f.toPath()).equals("image/jpeg") || Files.probeContentType(f.toPath()).equals("image/tiff") ){
				BufferedImage img = ImageIO.read(f) ;
				long nbSlices = (img.getType() == BufferedImage.TYPE_BYTE_GRAY || img.getType() == BufferedImage.TYPE_USHORT_GRAY ) ? 1 : 3 ;
				long height = img.getHeight(), width = img.getWidth() ;
				byte [] fileContent = Files.readAllBytes(f.toPath()) ;
				MimeType type = MimeType.valueOf(Files.probeContentType(f.toPath())) ;
				Image image = new Image(f.getName(), fileContent, width, height, nbSlices, new MediaType(type.getType(), type.getSubtype()), true) ;
				repository.save(image) ;
			  }
			}
		} catch (final IOException e) {
		  e.printStackTrace();
		}
    };
  }


}
