package pdl.backend;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Column;

import org.springframework.http.MediaType ;

import pdl.imageProcessing.EntryPointImageProcessing;

@Entity
public class Image {

  @GeneratedValue(strategy=GenerationType.AUTO)
  @Id
  private Long id ;
  private Long height, width, nbSlices, littleWidth, littleHeight;
  @Column(length = 2048)
  private String name;
  private String type, subtype ;
  @Lob
  private byte[] data;
  
  @Lob
  private byte [] reducedData ;


  protected Image() {}
  /**
   * Create an image
   * @param name the name of the image
   * @param data the bytes of the images
   * @param width the width of the image
   * @param height the height of the image
   * @param nbSlices the number of slices of the image
   * @param type the type of the image
   * @param isStored if the image is going to be stored. If yes, we create a reducedImage of this image
   */
  public Image(final String name, final byte[] data, Long width, Long height, Long nbSlices, MediaType type, boolean isStored) throws Exception {
    this.name = name;
    this.data = data;
    this.width = width ;
    this.height = height ;
    this.type = type.getType() ;
    this.subtype = type.getSubtype() ;
    this.nbSlices = nbSlices ;
    if (isStored){
      double factor = 50./ ( (double) getHeight()) ;
      reducedData = EntryPointImageProcessing.algorithmEntryPoint(this, new String [] {"resize", "factor="+(factor)}, null, false) ;
      littleWidth = (long) (getWidth()*factor) ;
      littleHeight = 50L ;
    }
    else
      reducedData = null ;
      littleWidth = -1L ;
      littleHeight = -1L ;
  }
  public Long getId() {
    return id;
  }
  public MediaType getType () {
    return new MediaType(type, subtype) ;
  }
  public long getWidth() {
    return width;
  }
  public long getHeight() {
    return height;
  }

  public long getLittleWidth() {
    return littleWidth;
  }
  public long getLittleHeight() {
    return littleHeight;
  }

  public long getNbSlices() {
    return  nbSlices;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public byte[] getData() {
    return data;
  }
  public byte [] getReducedImg() {
    return reducedData ;
  }
}
