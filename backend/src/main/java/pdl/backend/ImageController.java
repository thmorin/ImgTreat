package pdl.backend;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pdl.imageProcessing.EntryPointImageProcessing;
import pdl.imageProcessing.exception.NonExistingAlgorithmException;
import pdl.imageProcessing.exception.NonExistingParameterException;
import pdl.imageProcessing.exception.InvalidParametersException;
import org.springframework.util.MimeType ;

@RestController
public class ImageController {

  @Autowired
  private ObjectMapper mapper;

  @Autowired
  private ImageDao imageDao;

  /**
   * Getting an image of a given id. This method is a little all-in-one :
   * <ul>
   *  <li> if the get is just on <b> /images/id </b>, this method return the image without any treatment  </li>
   *  <li> if the get is on <b>/images/id?algorithm=X&p1=Y&p2=Z</b>, this method return the image corresponding to the treatment X, Y, Z </li>
   *  <li> if the get is on <b>/images/id?previewOneAlgorithm=X&p1=Y&p2=Z</b>, this method return the image corresponding to the treatment X, Y, Z on the little version of the image </li>
   *  <li> if the get is on <b>/images/id?algos=X</b>, this method return the image corresponding to the list of treatment of X</li>
   *  <li> if the get is on <b>/images/id?preview=X</b>, this method return the image corresponding to the list of treatment of X on the little version of the image</li>
   * </ul>
   * Note that if you make a get on two types of treatment (algorithm AND algos for example), this return an error
   */
  @RequestMapping(value = "/images/{id}", method = RequestMethod.GET)
  public ResponseEntity<?> getImage(@PathVariable("id") long id, @RequestParam(value = "algorithm", required = false) String algorithm, 
  @RequestParam(value ="previewOneAlgorithm", required = false) String previewOneAlgorithm,  @RequestParam(value = "p1", required = false) String param1,
  @RequestParam(value = "p2", required = false) String param2,  @RequestParam(value="algos", required = false) String algorithms,
  @RequestParam(value="preview", required = false) String previewAlgorithms) {
    boolean isOnReducedImage = false ;

    if (!imageDao.existsById(id))
      return new ResponseEntity<>("Image non trouvée", HttpStatus.NOT_FOUND) ;
    Image back = imageDao.getOne(id) ;
    if (algorithms == null && algorithm == null && previewAlgorithms == null && previewOneAlgorithm == null )
      return ResponseEntity.ok().contentType(back.getType()).body(back.getData()) ;

    int nbWithAlgo = (algorithms == null ? 0 : 1) + (algorithm == null ? 0 : 1) + (previewAlgorithms == null ? 0 : 1) + (previewOneAlgorithm == null ? 0 : 1) ;

    if (nbWithAlgo != 1 ){
      return new ResponseEntity<>("Url non valide", HttpStatus.BAD_REQUEST) ;
    }
    byte[] backData = null ;
    if (algorithm != null || previewOneAlgorithm != null) {
      // Un algorihme simple
      boolean isPreview = previewOneAlgorithm != null ;
      String valueAlgo = isPreview ? previewOneAlgorithm : algorithm ;
      try {
        backData =  EntryPointImageProcessing.algorithmEntryPoint(back, new String [] {valueAlgo, param1, param2}, imageDao, isPreview) ;
      } catch (InvalidParametersException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST) ;
      } catch (NonExistingAlgorithmException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST) ;
      } catch (NonExistingParameterException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST) ;
      } catch (Exception e) {
        e.printStackTrace();
        return new ResponseEntity<>("problème interne", HttpStatus.INTERNAL_SERVER_ERROR) ;
      } catch (Error e){
        e.printStackTrace();
        return new ResponseEntity<>("problème interne", HttpStatus.INTERNAL_SERVER_ERROR) ;
      }
    }
    else {
      if (previewAlgorithms != null){
        // On utilise l'image reduite
        isOnReducedImage = true ;
        algorithms = previewAlgorithms ;
      }
      // Plusieurs algorithmes
      String [] algos = algorithms.split(";") ;
      if (algos[0] == "") {
        //liste vide
        backData = isOnReducedImage ? back.getReducedImg() : back.getData() ;
      }
      else{
        for(String algo : algos){
          String [] params = algo.split(",") ;
          if (params[0] != null && (params[0].equals("add") || params[0].equals("substract") || params[0].equals("divide") || params[0].equals("resize")) ){
            return new ResponseEntity<>(" changer la taille de l'image est interdit", HttpStatus.BAD_REQUEST) ;
          }
          try {
            backData =  EntryPointImageProcessing.algorithmEntryPoint(back, params, imageDao, isOnReducedImage) ;
            back = new Image(back.getName(), backData, back.getWidth(), back.getHeight(), back.getNbSlices(), new MediaType(back.getType().getType(), back.getType().getSubtype()), false) ;
          } catch (InvalidParametersException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST) ;
          } catch (NonExistingAlgorithmException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST) ;
          } catch (NonExistingParameterException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST) ;
          } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("problème interne", HttpStatus.INTERNAL_SERVER_ERROR) ;
          } catch (Error e){
            e.printStackTrace();
            return new ResponseEntity<>("problème interne", HttpStatus.INTERNAL_SERVER_ERROR) ;
          }
          if (isOnReducedImage) isOnReducedImage = false ;
        }
      }
    }
    return ResponseEntity.ok().contentType(back.getType()).body(backData) ;
  }
  /**
   * delete the image corresponding to id
   */
  @RequestMapping(value = "/images/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteImage(@PathVariable("id") long id) {
    if (!imageDao.existsById(id))
      return new ResponseEntity<>("Image non trouvée", HttpStatus.NOT_FOUND) ;
    imageDao.deleteById(id) ;
    return ResponseEntity.ok().build() ;
  }
  /**
   * Post an image given on the body of the request
   */
  @RequestMapping(value = "/images", method = RequestMethod.POST)
  public ResponseEntity<?> addImage(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
      if (! (file.getContentType().equals("image/jpeg") || file.getContentType().equals("image/tiff"))){
        return new ResponseEntity<>("Le format n'est ni tiff, ni jpeg : "+ file.getContentType() , HttpStatus.UNSUPPORTED_MEDIA_TYPE) ;
      }
      if (file.isEmpty()){
        return new ResponseEntity<>("Fichier vide ..." , HttpStatus.UNPROCESSABLE_ENTITY) ;
      }
      try{
        BufferedImage image = ImageIO.read(file.getInputStream());
        long height = (long) image.getHeight();
        long width = (long) image.getWidth();
        long nbSlices = (image.getType() == BufferedImage.TYPE_BYTE_GRAY || image.getType() == BufferedImage.TYPE_USHORT_GRAY ) ? 1 : 3 ;
        MimeType type = MimeType.valueOf(file.getContentType()) ;
        Image img = new Image(file.getResource().getFilename(), file.getBytes(), width, height, nbSlices, new MediaType(type.getType(), type.getSubtype()), true) ;
        imageDao.save(img);
      }catch (Exception e){
        return new ResponseEntity<>("Problème interne ..." , HttpStatus.INTERNAL_SERVER_ERROR) ;
      }
    return ResponseEntity.status(HttpStatus.CREATED).build() ;
  }
  /**
   * getting the list of the images which are on the database. This will return all the metadata corresponding
   */
  @RequestMapping(value = "/images", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
  @ResponseBody
  public ArrayNode getImageList() {
    ArrayNode nodes = mapper.createArrayNode();
    for (Image img: imageDao.findAll()){
      nodes.add(mapper.valueToTree(new ImageInfos(img.getName(), img.getId(), img.getWidth(), img.getHeight(), img.getNbSlices(), img.getType()))) ;
    }
    return nodes;
  }
  /**
   * Getting the metadata corresponding to the image id
   */
 @RequestMapping(value = "/images/infos/{id}", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
  public ResponseEntity<?> getImageInfo(@PathVariable("id") Long id) {
    if (!imageDao.existsById(id))
      return new ResponseEntity<>("Image non trouvée", HttpStatus.NOT_FOUND) ;
    Image back = imageDao.getOne(id) ;

    JsonNode node = mapper.valueToTree(new ImageInfos(back.getName(), back.getId(), back.getWidth(), back.getHeight(), back.getNbSlices(), back.getType())) ;
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(node);
  }

}
