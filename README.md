# ImgTreat

## Introduction

ImgTreat est une application type client serveur spécialisée dans le traitement d'image. Côté backend, elle est écrite en java, avec le framework spring-boot pour la partie serveur et la bibliothèque imglib2 pour la partie traitements d'images. Côté frontend, elle est écrite avec l'aide du framework Vue.js. Nous avons testé le serveur sur Ubuntu 20.04.2 et Ubuntu 18.04. Comme navigateur nous avons testé sur Firefox 87.0, Google Chrome 89.0 et Safari 14.0.3.

## Démarrer le projet

Pour démarrer le projet, vous devez lancer le backend en utilisant cette [vscode extension](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-spring-boot-dashboard) ou en utilisant cette commande  `mvn spring-boot:run`. Vous devez aussi lancer le frontend en utilisant la commande `npm install` (pour installer les dépendances) puis la commande `npm run serve`. Vous devez aussi au préalable lancer la commande `mvn clean install`

## Se connecter au site web

Vous pouvez vous connecter au site web à l'adresse : <http://localhost:8089/>. Vous pouvez aussi vous connecter directement au serveur à l'adresse : <http://localhost:8081/>.

## Utiliser l'applicatoin ImgTreat

Pour avoir plus d'information sur l'utilisation de l'application vous pouvez appuyer sur le bouton Aide présent en haut de toutes les pages.

